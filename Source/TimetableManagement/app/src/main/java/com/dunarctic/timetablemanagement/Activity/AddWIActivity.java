package com.dunarctic.timetablemanagement.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;
import com.dunarctic.timetablemanagement.Utility.Utility;
import com.nex3z.togglebuttongroup.MultiSelectToggleGroup;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import eltos.simpledialogfragment.SimpleDialog;
import eltos.simpledialogfragment.color.SimpleColorDialog;

import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;

/**
 * Created by dunarctic on 02.12.2017
 */

public class AddWIActivity extends AppCompatActivity
        implements SimpleDialog.OnDialogResultListener {
    @BindView(R.id.et_title)
    EditText title;
    @BindView(R.id.et_description)
    EditText description;
    @BindView(R.id.btn_date)
    Button btnDate; //used for Start Date in Class and Holiday, used for End Date in Deadline
    @BindView(R.id.btn_due_time)
    Button btnDueTime; //only used for End Time in Deadline
    @BindView(R.id.btn_start_time)
    Button btnStartTime; //only used for Start Time in Class
    @BindView(R.id.btn_end_time)
    Button btnEndTime; //only used for End Time in Class
    @BindView(R.id.btn_repeat)
    Button btnWeekdaysRepeat; //only used in Class
    @BindView(R.id.btn_end_date)
    Button btnEndDate; //only used for End Date in Holiday
    @BindView(R.id.btn_reminder)
    Button btnReminder;
    @BindView(R.id.btn_choose_color)
    Button btnColor;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.toolbar_wi)
    Toolbar toolbar;
    @BindView(R.id.radio_group_type)
    RadioRealButtonGroup radio_group;
    @BindView(R.id.group_weekdays)
    MultiSelectToggleGroup multi_weekdays;
    @BindView(R.id.layout_deadline)
    LinearLayout layout_deadline;
    @BindView(R.id.layout_class)
    LinearLayout layout_class;
    @BindView(R.id.layout_holiday)
    LinearLayout layout_holiday;
    private Context context;
    //0: Deadline; 1: Class; 2: Holiday //<---add to database (7)
    private int curType = DEADLINE_TYPE;
    //also used for due date
    private int mStartYear, mStartMonth, mStartDayOfMonth, mStartHour, mStartMinute,
            mEndYear, mEndMonth, mEndDayOfMonth, mEndHour, mEndMinute;
    private boolean isSelectedTime = false, isSelectedDate = false,
            isSelectedStartTime = false;
    private int mWeeks = 0, mTypeReminder = 0, mReminderValue = 1, mReminderChoice = 2;
    private int mColor = 0; //<---add to database (6)
    private boolean weekdaysSelection[] = {false, false, false, false, false, false, false};
    private AlertDialog alert;
    private AlertDialog custom;
    private AlertDialog weeksSet;
    private WorkItemDatabaseHelper mdbHelper;
    private DateTimeUtility dtFormat;
    private String COLOR_DIALOG = "CHOOSE_COLOR";
    private ReminderUtility reminderUti;

    //0: No notification
    //1: Use minutes before -> use notifyMore
    //2: Use hours before -> use notifyMore
    //3: Use days before -> use notifyMore
    //4: Use weeks before -> use notifyMore
    //5: At due time
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_workitem);
        ButterKnife.bind(this);
        context = this;
        dtFormat = new DateTimeUtility();
        reminderUti = new ReminderUtility(context);
        mdbHelper = new WorkItemDatabaseHelper(context);
        // handle tool bar
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        mColor = getResources().getColor(R.color.colorPrimaryDark);
        toolbar.setBackgroundColor(mColor);
        btnColor.setBackgroundColor(mColor);

        //create back button on top-left of toolbar
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // deadline layout is set as default

        layout_class.setVisibility(View.GONE);
        layout_holiday.setVisibility(View.GONE);
        // onPositionChanged listener detects if there is any change in position
        // and then change layout visibility
        radio_group.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {
                curType = currentPosition;
                switch (curType) {
                    case DEADLINE_TYPE:
                        layout_deadline.animate().alpha(1.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_deadline.setVisibility(View.VISIBLE);
                                    }
                                });
                        layout_class.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_class.setVisibility(View.GONE);
                                    }
                                });
                        layout_holiday.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_holiday.setVisibility(View.GONE);
                                    }
                                });
                        break;
                    case CLASS_TYPE:
                        layout_deadline.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_deadline.setVisibility(View.GONE);
                                    }
                                });
                        layout_class.animate().alpha(1.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_class.setVisibility(View.VISIBLE);
                                    }
                                });
                        layout_holiday.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_holiday.setVisibility(View.GONE);
                                    }
                                });
                        break;
                    case HOLIDAY_TYPE:
                        layout_deadline.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_deadline.setVisibility(View.GONE);
                                    }
                                });
                        layout_class.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_class.setVisibility(View.GONE);
                                    }
                                });
                        layout_holiday.animate().alpha(1.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_holiday.setVisibility(View.VISIBLE);
                                    }
                                });
                        break;
                }
                resetValues();
            }
        });
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(context, new DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int
                            selectedMonth, int selectedDay) {
                        Calendar cal = Calendar.getInstance();
                        Date now = cal.getTime();
                        if (curType != DEADLINE_TYPE) //holiday is not have time set,
                            // this holiday will take all day
                            cal.set(selectedYear, selectedMonth, selectedDay, 0, 0);
                        else
                            cal.set(selectedYear, selectedMonth, selectedDay, 23, 59);
                        Date dateSet = cal.getTime();
                        if (now.compareTo(dateSet) <= 0) {
                            switch (curType) {
                                case DEADLINE_TYPE: //current type is deadline
                                    mEndYear = selectedYear;
                                    mEndMonth = selectedMonth;
                                    mEndDayOfMonth = selectedDay;
                                    mEndHour = 0;
                                    mEndMinute = 0;
                                    btnDueTime.setText(R.string.due_time);
                                    btnDate.setText(dtFormat.dateToString(cal));
                                    break;
                                case CLASS_TYPE: //current type is class
                                    mEndYear = mStartYear = selectedYear;
                                    mEndMonth = mStartMonth = selectedMonth;
                                    mEndDayOfMonth = mStartDayOfMonth = selectedDay;
                                    mEndHour = mStartHour = 0;
                                    mEndMinute = mStartMinute = 0;
                                    btnStartTime.setText(getResources().getString(R.string.start_time));
                                    btnEndTime.setText(getResources().getString(R.string.end_time));
                                    btnDate.setText(dtFormat.weekToString(cal));
                                    break;
                                case HOLIDAY_TYPE: //current type is holiday
                                    mStartYear = selectedYear;
                                    mStartMonth = selectedMonth;
                                    mStartDayOfMonth = selectedDay;
                                    mEndHour = mStartHour = 0;
                                    mEndMinute = mStartMinute = 0;
                                    mEndYear = 0;
                                    mEndMonth = 0;
                                    mEndDayOfMonth = 0;
                                    btnEndDate.setText(R.string.end_date);
                                    btnDate.setText(dtFormat.reminderToString(cal));
                                    break;
                            }
                            mTypeReminder = 0;
                            isSelectedDate = true;
                            isSelectedTime = false;
                            isSelectedStartTime = false;
                            btnReminder.setText(getResources().getString(R.string.
                                    add_reminder));
                        } else {
                            if (curType == DEADLINE_TYPE)
                                Toast.makeText(getApplicationContext(), "Due date is not valid",
                                        Toast.LENGTH_SHORT).show();
                            else if (curType == HOLIDAY_TYPE)
                                Toast.makeText(getApplicationContext(),
                                        "Start date cannot be today or in the past", Toast
                                                .LENGTH_SHORT).show();
                            else
                                Toast.makeText(getApplicationContext(), "Class must be set from " +
                                        "tomorrow", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });
        //only used for Holiday object
        btnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSelectedDate) {
                    Toast.makeText(getApplicationContext(), "Please set Start date first",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(context, new DatePickerDialog
                        .OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int
                            selectedMonth, int selectedDay) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(mStartYear, mStartMonth,
                                mStartDayOfMonth,
                                mStartHour, mStartMinute);
                        Date start = cal.getTime();
                        cal.set(selectedYear, selectedMonth, selectedDay, 23, 59);
                        Date end = cal.getTime();
                        if (start.compareTo(end) <= 0) {
                            mEndYear = selectedYear;
                            mEndMonth = selectedMonth;
                            mEndDayOfMonth = selectedDay;
                            mEndHour = 23;
                            mEndMinute = 59;
                            mTypeReminder = 0;
                            isSelectedTime = true;
                            btnReminder.setText(getResources().getString(R.string.
                                    add_reminder));
                            btnEndDate.setText(dtFormat.reminderToString(cal));
                        } else
                            Toast.makeText(getApplicationContext(), "End Date is not valid",
                                    Toast.LENGTH_SHORT).show();
                    }
                }, mStartYear, mStartMonth, mStartDayOfMonth);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
            }
        });
        //only used for Deadline object
        btnDueTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSelectedDate) {
                    Toast.makeText(getApplicationContext(), "Please set Due date first",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                final Calendar calendar = Calendar.getInstance();
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Calendar cal = Calendar.getInstance();
                        Date now = cal.getTime();
                        cal = Calendar.getInstance();
                        cal.set(mEndYear, mEndMonth,
                                mEndDayOfMonth,
                                mEndHour, mEndMinute);
                        cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                        cal.set(Calendar.MINUTE, selectedMinute);
                        Date dateSet = cal.getTime();
                        if (now.compareTo(dateSet) <= 0) {
                            mEndHour = selectedHour;
                            mEndMinute = selectedMinute;
                            mTypeReminder = 0;
                            isSelectedTime = true;
                            btnReminder.setText(getResources().getString(R.string.
                                    add_reminder));
                            btnDueTime.setText(dtFormat.timeToString(cal));
                        } else
                            Toast.makeText(getApplicationContext(), "Due time is not valid",
                                    Toast.LENGTH_SHORT).show();
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), 0, true);//true for 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        //only used for Class object
        btnStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSelectedDate) {
                    Toast.makeText(getApplicationContext(), "Please set Start week first",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                final Calendar calendar = Calendar.getInstance();
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        //no need to check valid
                        mStartHour = selectedHour;
                        mStartMinute = selectedMinute;
                        Calendar cal = Calendar.getInstance();
                        cal.set(mStartYear, mStartMonth,
                                mStartDayOfMonth,
                                mStartHour, mStartMinute);
                        mTypeReminder = 0;
                        isSelectedStartTime = true;
                        btnReminder.setText(getResources().getString(R.string.
                                add_reminder));
                        btnStartTime.setText(dtFormat.timeToString(cal));
                        btnEndTime.setText(getResources().getString(R.string.
                                end_time));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), 0, true);//true for 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        //only used for Class object
        btnEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSelectedStartTime) {
                    Toast.makeText(getApplicationContext(), "Please set Start time first",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                final Calendar calendar = Calendar.getInstance();
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(mStartYear, mStartMonth,
                                mStartDayOfMonth,
                                mStartHour,
                                mStartMinute);
                        Date start = cal.getTime();
                        cal.set(mEndYear, mEndMonth,
                                mEndDayOfMonth, selectedHour, selectedMinute);
                        Date end = cal.getTime();
                        if (start.compareTo(end) <= 0) {
                            long diff = end.getTime() - start.getTime();
                            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
                            if (minutes >= 50) {
                                mEndHour = selectedHour;
                                mEndMinute = selectedMinute;
                                cal = Calendar.getInstance();
                                cal.set(mEndYear, mEndMonth,
                                        mEndDayOfMonth,
                                        mEndHour, mEndMinute);
                                mTypeReminder = 0;
                                isSelectedTime = true;
                                btnReminder.setText(getResources().getString(R.string.
                                        add_reminder));
                                btnEndTime.setText(dtFormat.timeToString(cal));
                            } else
                                Toast.makeText(getApplicationContext(), "Class must last" +
                                                " at least 50 minutes",
                                        Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getApplicationContext(), "End time must be after " +
                                            "Start time",
                                    Toast.LENGTH_SHORT).show();
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), 0, true);//true for 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        //only used for Class object
        btnWeekdaysRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View promptsView = LayoutInflater.from(context).inflate(R.layout.input_one_value, null);
                final ViewHolderInputValue viewHolderInput = new ViewHolderInputValue(promptsView);
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                viewHolderInput.userInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (charSequence.toString().isEmpty() || Integer.parseInt
                                (charSequence.toString()) == 0) {
                            viewHolderInput.btn_set.setEnabled(false);
                        } else {
                            viewHolderInput.btn_set.setEnabled(true);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
                viewHolderInput.btn_set.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWeeks = Integer.parseInt(viewHolderInput.userInput.getText().toString());
                        String temp = "Number of weeks: ";
                        temp += viewHolderInput.userInput.getText().toString();
                        btnWeekdaysRepeat.setText(temp);
                        weeksSet.dismiss();
                    }
                });
                builder.setView(promptsView);

                // set dialog message
                weeksSet = builder.create();
                weeksSet.show();
            }
        });
        multi_weekdays.setOnCheckedChangeListener(new MultiSelectToggleGroup.OnCheckedStateChangeListener() {
            @Override
            public void onCheckedStateChanged(MultiSelectToggleGroup group, int checkedId, boolean isChecked) {
                int ID = checkedId % 7;
                if (ID == 0)
                    ID = 7;
                if (ID >= 1 && ID <= 7) {
                    weekdaysSelection[ID - 1] = isChecked;
                }
            }
        });
        btnReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSelectedTime) {
                    switch (curType) {
                        case DEADLINE_TYPE:
                            Toast.makeText(getApplicationContext(),
                                    "Please set Due time first", Toast.LENGTH_SHORT).show();
                            break;
                        case CLASS_TYPE:
                            Toast.makeText(getApplicationContext(),
                                    "Please set Time first", Toast.LENGTH_SHORT).show();
                            break;
                        case HOLIDAY_TYPE:
                            Toast.makeText(getApplicationContext(),
                                    "Please set Start and End date first",
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }
                    return;
                }
                final View promptsView = LayoutInflater.from(context).inflate(R.layout.dialog_set_reminder, null);
                final ViewHolderReminder viewHolderReminder = new ViewHolderReminder(promptsView);
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);

                switch (mTypeReminder) {
                    case 0:
                        viewHolderReminder.iv_no_notification.setVisibility(View.VISIBLE);
                        viewHolderReminder.btn_no_notification
                                .setTextColor(getResources().getColor(R.color.notifyColor));
                        break;
                    case 5:
                        viewHolderReminder.iv_at_due.setVisibility(View.VISIBLE);
                        viewHolderReminder.btn_at_due.setTextColor(getResources()
                                .getColor(R.color.notifyColor));
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        viewHolderReminder.iv_more.setVisibility(View.VISIBLE);
                        viewHolderReminder.btn_more.setTextColor(getResources()
                                .getColor(R.color.notifyColor));
                        break;
                }
                switch (curType) {
                    case DEADLINE_TYPE:
                        viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                at_due));
                        break;
                    case CLASS_TYPE:
                        viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                at_start_time));
                        break;
                    case HOLIDAY_TYPE:
                        viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                at_start_date));
                        break;
                }
                setMoreOption(viewHolderReminder);
                viewHolderReminder.btn_no_notification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTypeReminder = 0;
                        btnReminder.setText(getResources().getString(R.string.
                                add_reminder));
                        alert.dismiss();
                    }
                });
                viewHolderReminder.btn_at_due.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTypeReminder = 5;
                        Calendar cal = Calendar.getInstance();
                        switch (curType) {
                            case DEADLINE_TYPE:
                                cal.set(mEndYear, mEndMonth,
                                        mEndDayOfMonth,
                                        mEndHour, mEndMinute);
                                btnReminder.setText(getReminderStr(cal));
                                break;
                            case CLASS_TYPE:
                                btnReminder.setText(getResources().getString(R.string.at_start_time));
                                break;
                            case HOLIDAY_TYPE:
                                cal.set(mStartYear, mStartMonth,
                                        mStartDayOfMonth,
                                        mStartHour, mStartMinute);
                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                btnReminder.setText(getReminderStr(cal));
                                break;
                        }
                        alert.dismiss();
                    }
                });
                viewHolderReminder.btn_more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTypeReminder = mReminderChoice; //IMPORTANT
                        Calendar cal = Calendar.getInstance();
                        switch (curType) {
                            case DEADLINE_TYPE:
                                cal.set(mEndYear, mEndMonth,
                                        mEndDayOfMonth,
                                        mEndHour, mEndMinute);
                                minusCalendar(cal, mTypeReminder, mReminderValue);
                                btnReminder.setText(getReminderStr(cal));
                                break;
                            case CLASS_TYPE:
                                btnReminder.setText(getMoreStr());
                                break;
                            case HOLIDAY_TYPE:
                                cal.set(mStartYear, mStartMonth,
                                        mStartDayOfMonth,
                                        mStartHour, mStartMinute);
                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                cal.set(Calendar.MINUTE, 0);
                                minusCalendar(cal, mTypeReminder, mReminderValue);
                                btnReminder.setText(getReminderStr(cal));
                                break;
                        }
                        alert.dismiss();
                    }
                });
                viewHolderReminder.btn_custom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ViewHolderCustom viewHolderCustom;
                        View promptsViewCustom = LayoutInflater.from(context).inflate(R.layout.dialog_custom_notification, null);
                        viewHolderCustom = new ViewHolderCustom(promptsViewCustom);
                        final AlertDialog.Builder builderCustom = new AlertDialog.Builder(context);
                        //turn on specific button and image view
                        switchCustom(viewHolderCustom, mReminderChoice);
                        viewHolderCustom.userInput.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                if (charSequence.toString().isEmpty() || Integer.parseInt
                                        (charSequence.toString()) == 0) {
                                    viewHolderCustom.btn_set.setEnabled(false);
                                } else {
                                    viewHolderCustom.btn_set.setEnabled(true);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                            }
                        });
                        viewHolderCustom.btn_minutes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mReminderChoice = 1;
                                //turn on specific button and image view
                                switchCustom(viewHolderCustom, mReminderChoice);
                            }
                        });
                        viewHolderCustom.btn_hours.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mReminderChoice = 2;
                                //turn on specific button and image view
                                switchCustom(viewHolderCustom, mReminderChoice);
                            }
                        });
                        viewHolderCustom.btn_days.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mReminderChoice = 3;
                                //turn on specific button and image view
                                switchCustom(viewHolderCustom, mReminderChoice);
                            }
                        });
                        viewHolderCustom.btn_weeks.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mReminderChoice = 4;
                                //turn on specific button and image view
                                switchCustom(viewHolderCustom, mReminderChoice);
                            }
                        });
                        viewHolderCustom.btn_set.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTypeReminder = mReminderChoice;
                                mReminderValue = Integer.parseInt(viewHolderCustom.userInput.getText().toString());
                                Calendar cal = Calendar.getInstance();
                                switch (curType) {
                                    case DEADLINE_TYPE:
                                        cal.set(mEndYear, mEndMonth,
                                                mEndDayOfMonth,
                                                mEndHour, mEndMinute);
                                        minusCalendar(cal, mTypeReminder, mReminderValue);
                                        btnReminder.setText(getReminderStr(cal));
                                        break;
                                    case CLASS_TYPE:
                                        btnReminder.setText(getMoreStr());
                                        break;
                                    case HOLIDAY_TYPE:
                                        cal.set(mStartYear, mStartMonth,
                                                mStartDayOfMonth,
                                                mStartHour, mStartMinute);
                                        cal.set(Calendar.HOUR_OF_DAY, 0);
                                        cal.set(Calendar.MINUTE, 0);
                                        minusCalendar(cal, mTypeReminder, mReminderValue);
                                        btnReminder.setText(getReminderStr(cal));
                                        break;
                                }
                                custom.dismiss();
                                alert.dismiss();
                            }
                        });
                        builderCustom.setView(promptsViewCustom);

                        // set dialog message
                        custom = builderCustom.create();
                        custom.show();
                    }
                });
                //ImageView iv_custom = (ImageView) promptsView.findViewById(R.id.iv_custom);// not used
                builder.setView(promptsView);
                alert = builder.create();
                alert.show();
            }
        });
        btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleColorDialog.build()
                        .title(R.string.choose_color)
                        .colorPreset(mColor)
                        .allowCustom(true)
                        .show(AddWIActivity.this, COLOR_DIALOG);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateUserInput())
                    return;
                String titleStr = title.getText().toString(); //<---add to database (1)(4:false)
                String descriptionStr = description.getText().toString(); //<---add to database (8)
                String startDateStr, endDateStr, reminderStr;
                Calendar cal;
                //add work item to database
                WorkItem witem;
                int notificationNumber = Utility.getUniqueNumber(context);
                switch (curType) {
                    case DEADLINE_TYPE:
                        if (mEndHour == 0 && mEndMinute == 0)
                            mEndMinute = 1;
                        cal = Calendar.getInstance();
                        cal.set(mEndYear, mEndMonth,
                                mEndDayOfMonth,
                                mEndHour, mEndMinute, 0);
                        endDateStr = dtFormat.dateTimeToString(cal); //<---add to database (3)
                        startDateStr = endDateStr; //<---add to database (2)
                        if (mTypeReminder != 0) {
                            minusCalendar(cal, mTypeReminder, mReminderValue); //save reminder time
                            reminderStr = dtFormat.dateTimeToString(cal); //<---add to database (5)
                        } else
                            reminderStr = "";
                        mdbHelper.insert(titleStr, descriptionStr, startDateStr, endDateStr,
                                reminderStr, mColor, curType, notificationNumber, context);
                        if (mTypeReminder != 0) {
                            Notification set =
                                    reminderUti.getNotification(titleStr, cal, curType, mColor);
                            reminderUti.setNotification(set, notificationNumber, cal);
                        }
                        break;
                    case CLASS_TYPE:
                        if (mStartHour == 0 && mStartMinute == 0)
                            mStartMinute = 1;
                        Calendar mStartCalendar = Calendar.getInstance();
                        Calendar mEndCalendar = Calendar.getInstance();
                        Calendar tempStart, tempEnd;
                        mStartCalendar.set(mStartYear, mStartMonth, mStartDayOfMonth,
                                mStartHour, mStartMinute);
                        mEndCalendar.set(mEndYear, mEndMonth, mEndDayOfMonth,
                                mEndHour, mEndMinute);
                        for (int idWeek = 0; idWeek < mWeeks; ++idWeek) {
                            tempStart = Calendar.getInstance();
                            tempStart.set(mStartCalendar.get(Calendar.YEAR),
                                    mStartCalendar.get(Calendar.MONTH),
                                    mStartCalendar.get(Calendar.DAY_OF_MONTH),
                                    mStartCalendar.get(Calendar.HOUR_OF_DAY),
                                    mStartCalendar.get(Calendar.MINUTE));
                            tempEnd = Calendar.getInstance();
                            tempEnd.set(mEndCalendar.get(Calendar.YEAR),
                                    mEndCalendar.get(Calendar.MONTH),
                                    mEndCalendar.get(Calendar.DAY_OF_MONTH),
                                    mEndCalendar.get(Calendar.HOUR_OF_DAY),
                                    mEndCalendar.get(Calendar.MINUTE));
                            tempStart.setFirstDayOfWeek(Calendar.MONDAY);
                            tempEnd.setFirstDayOfWeek(Calendar.MONDAY);
                            int idWeekday = 0;
                            for (boolean item : weekdaysSelection) {
                                //if there are any checked day
                                if (item) {
                                    dtFormat.changeWeekdays(tempStart, idWeekday);
                                    dtFormat.changeWeekdays(tempEnd, idWeekday);
                                    startDateStr = dtFormat.dateTimeToString(tempStart); //<---add to database (2)
                                    endDateStr = dtFormat.dateTimeToString(tempEnd); //<---add to database (3)
                                    if (mTypeReminder != 0) {
                                        minusCalendar(tempStart, mTypeReminder, mReminderValue); //save reminder time
                                        reminderStr = dtFormat.dateTimeToString(tempStart); //<---add to database (5)
                                    } else
                                        reminderStr = "";
                                    mdbHelper.insert(titleStr, descriptionStr, startDateStr,
                                            endDateStr,
                                            reminderStr, mColor, curType, notificationNumber,
                                            context);
                                    //>>>>>>>>>>>>>>>>ADD REMIDER<<<<<<<<<<<<<<<<<<
                                    if (mTypeReminder != 0) {
                                        Notification set =
                                                reminderUti.getNotification(titleStr, tempStart, curType, mColor);
                                        reminderUti.setNotification(set, notificationNumber, tempStart);
                                    }
                                }
                                idWeekday++;
                            }
                            mStartCalendar.add(Calendar.DAY_OF_MONTH, +7);
                            mEndCalendar.add(Calendar.DAY_OF_MONTH, +7);
                        }
                        break;
                    case HOLIDAY_TYPE:
                        if (mStartHour == 0 && mStartMinute == 0)
                            mStartMinute = 1;
                        cal = Calendar.getInstance();
                        cal.set(mStartYear, mStartMonth,
                                mStartDayOfMonth,
                                mStartHour, mStartMinute);
                        startDateStr = dtFormat.dateTimeToString(cal); //<---add to database (2)
                        cal = Calendar.getInstance();
                        cal.set(mEndYear, mEndMonth,
                                mEndDayOfMonth,
                                mEndHour, mEndMinute);
                        endDateStr = dtFormat.dateTimeToString(cal); //<---add to database (3)
                        if (mTypeReminder != 0) {
                            cal = Calendar.getInstance();
                            cal.set(mStartYear, mStartMonth,
                                    mStartDayOfMonth,
                                    mStartHour, mStartMinute);
                            minusCalendar(cal, mTypeReminder, mReminderValue); //save reminder time
                            reminderStr = dtFormat.dateTimeToString(cal); //<---add to database (5)
                        } else
                            reminderStr = "";
                        mdbHelper.insert(titleStr, descriptionStr, startDateStr, endDateStr,
                                reminderStr, mColor, curType, notificationNumber, context);
                        //>>>>>>>>>>>>>>>>ADD REMINDER<<<<<<<<<<<<<<<<<<
                        if (mTypeReminder != 0) {
                            Notification set =
                                    reminderUti.getNotification(titleStr, cal, curType, mColor);
                            reminderUti.setNotification(set, notificationNumber, cal);
                        }
                        break;
                }
                Toast.makeText(getApplicationContext(), "Add a work-item successfully ",
                        Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public boolean onResult(@NonNull String dialogTag, int which, @NonNull Bundle extras) {
        if (which == BUTTON_POSITIVE && COLOR_DIALOG.equals(dialogTag)) {
            mColor = extras.getInt(SimpleColorDialog.COLOR);
            toolbar.setBackgroundColor(mColor);
            btnColor.setBackgroundColor(mColor);
            return true;
        }
        return false;
    }

    public String getMoreStr() {
        String customStr = mReminderValue + " ";
        switch (mReminderChoice) {
            case 1:
                customStr += getResources().getString(R.string.minutes_before);
                break;
            case 2:
                customStr += getResources().getString(R.string.hours_before);
                break;
            case 3:
                customStr += getResources().getString(R.string.days_before);
                break;
            case 4:
                customStr += getResources().getString(R.string.weeks_before);
                break;
        }
        return customStr;
    }

    public void setMoreOption(ViewHolderReminder viewHolderReminder) {
        String customStr = getMoreStr();
        switch (mReminderChoice) {
            case 1:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 2:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 3:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 4:
                viewHolderReminder.btn_more.setText(customStr);
                break;
        }
    }

    public void resetValues() {
        mStartYear = mStartMonth = mStartDayOfMonth = mStartHour = mStartMinute = 0;
        mEndYear = mEndMonth = mEndDayOfMonth = mEndHour = mEndMinute = 0;
        mWeeks = 0;

        isSelectedTime = false;
        isSelectedDate = false;
        isSelectedStartTime = false;

        mTypeReminder = 0;
        mReminderValue = 1;
        mReminderChoice = 2;
        if (checkValidWeekdaysSelection()) {
            multi_weekdays.clearCheck();
        }

        //reset interface of all items in changeable layout
        if (curType == DEADLINE_TYPE)
            btnDate.setText(getResources().getString(R.string.due_date));
        else if (curType == CLASS_TYPE)
            btnDate.setText(getResources().getString(R.string.start_week));
        else
            btnDate.setText(getResources().getString(R.string.start_date));
        btnDueTime.setText(getResources().getString(R.string.due_time));
        btnEndDate.setText(getResources().getString(R.string.end_date));
        btnStartTime.setText(getResources().getString(R.string.start_time));
        btnEndTime.setText(getResources().getString(R.string.end_time));
        btnWeekdaysRepeat.setText(getResources().getString(R.string.week_class));
        btnReminder.setText(getResources().getString(R.string.add_reminder));
    }

    public boolean checkValidWeekdaysSelection() {
        for (boolean item : weekdaysSelection) {
            //if there are any checked day
            if (item)
                return true;
        }
        return false;
    }

    /* return a string to add into button reminder*/
    public boolean validateUserInput() {
        String curTitle = title.getText().toString();
        if (curTitle.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Title should not be empty",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if (curTitle.contains("'")) {
            Toast.makeText(getApplicationContext(), "Title contains special character",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!description.getText().toString().isEmpty()) {
            if (description.getText().toString().contains("'")) {
                Toast.makeText(getApplicationContext(), "Notes contains special character",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (mdbHelper.checkExist(curTitle, curType, context)) {
            Toast.makeText(getApplicationContext(), "You've already had this item",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!isSelectedTime) {
            Toast.makeText(getApplicationContext(), "Please select both date and time",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        switch (curType) {
            case DEADLINE_TYPE:
                cal = Calendar.getInstance();
                cal.set(mEndYear, mEndMonth,
                        mEndDayOfMonth,
                        mEndHour, mEndMinute);
                break;
            case CLASS_TYPE:
                if (mWeeks == 0) {
                    Toast.makeText(getApplicationContext(), "Please select number of weeks",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (!checkValidWeekdaysSelection()) {
                    Toast.makeText(getApplicationContext(), "Please select at least 1 weekdays " +
                            "of class", Toast.LENGTH_SHORT).show();
                    return false;
                }
                cal = Calendar.getInstance();
                cal.set(mStartYear, mStartMonth,
                        mStartDayOfMonth,
                        mStartHour, mStartMinute);
                break;
            case HOLIDAY_TYPE:
                cal = Calendar.getInstance();
                cal.set(mStartYear, mStartMonth,
                        mStartDayOfMonth,
                        mStartHour, mStartMinute);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                break;
        }
        Date dateStart = cal.getTime(); // save due time
        if (now.compareTo(dateStart) >= 0) {
            //if due time is a point of time in the past
            Toast.makeText(getApplicationContext(), "Please update date and time",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        Date reminderStart;
        if (mTypeReminder != 0) {
            minusCalendar(cal, mTypeReminder, mReminderValue); //save reminder time
            reminderStart = cal.getTime();
            if (now.compareTo(reminderStart) >= 0) {
                Toast.makeText(getApplicationContext(), "Reminder is not valid",
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    /* return a string to add into button reminder*/
    public String getReminderStr(Calendar calendar) {
        return "Reminder set for " + dtFormat.reminderToString(calendar);
    }

    /*  type = 1 : apply to MINUTE
        type = 2 : apply to HOUR
        type = 3 : apply to DAY_OF_MONTH
        type = 4 : apply to WEEK_OF_YEAR */
    public void minusCalendar(Calendar calendar, int type, int value) {
        switch (type) {
            case 1:
                calendar.add(Calendar.MINUTE, -value);
                break;
            case 2:
                calendar.add(Calendar.HOUR_OF_DAY, -value);
                break;
            case 3:
                calendar.add(Calendar.DAY_OF_MONTH, -value);
                break;
            case 4:
                calendar.add(Calendar.WEEK_OF_YEAR, -value);
                break;
            default: //not change
                break;
        }
    }

    /*  onId = 1 : only 1st attributes is visible
        onId = 2 : only 2nd attributes is visible
        onId = 3 : only 3rd attributes is visible
        onId = 4 : only 4th attributes is visible */
    public void switchCustom(ViewHolderCustom view, int onId) {
        Resources res = getResources();
        view.iv_minutes.setVisibility((onId == 1) ? View.VISIBLE : View.INVISIBLE);
        view.btn_minutes.setTextColor((onId == 1) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_minutes.setText((onId == 1) ? res.getString(R.string
                .minutes_before) : res.getString(R.string
                .minutes));

        view.iv_hours.setVisibility((onId == 2) ? View.VISIBLE : View.INVISIBLE);
        view.btn_hours.setTextColor((onId == 2) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_hours.setText((onId == 2) ? res.getString(R.string
                .hours_before) : res.getString(R.string
                .hours));

        view.iv_days.setVisibility((onId == 3) ? View.VISIBLE : View.INVISIBLE);
        view.btn_days.setTextColor((onId == 3) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_days.setText((onId == 3) ? res.getString(R.string
                .days_before) : res.getString(R.string
                .days));

        view.iv_weeks.setVisibility((onId == 4) ? View.VISIBLE : View.INVISIBLE);
        view.btn_weeks.setTextColor((onId == 4) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_weeks.setText((onId == 4) ? res.getString(R.string
                .weeks_before) : res.getString(R.string
                .weeks));
    }

    /* Use view holder for Reminder */
    static class ViewHolderReminder {
        @BindView(R.id.iv_no_notification)
        ImageView iv_no_notification;
        @BindView(R.id.btn_no_notification)
        Button btn_no_notification;
        @BindView(R.id.iv_at_due)
        ImageView iv_at_due;
        @BindView(R.id.btn_at_due)
        Button btn_at_due;
        @BindView(R.id.iv_more)
        ImageView iv_more;
        @BindView(R.id.btn_more)
        Button btn_more;
        @BindView(R.id.btn_custom)
        Button btn_custom;

        private ViewHolderReminder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    /* Use view holder for Custom reminder */
    static class ViewHolderCustom {
        @BindView(R.id.iv_minutes)
        ImageView iv_minutes;
        @BindView(R.id.btn_minutes)
        Button btn_minutes;
        @BindView(R.id.iv_hours)
        ImageView iv_hours;
        @BindView(R.id.btn_hours)
        Button btn_hours;
        @BindView(R.id.iv_days)
        ImageView iv_days;
        @BindView(R.id.btn_days)
        Button btn_days;
        @BindView(R.id.iv_weeks)
        ImageView iv_weeks;
        @BindView(R.id.btn_weeks)
        Button btn_weeks;
        @BindView(R.id.et_value)
        EditText userInput;
        @BindView(R.id.btn_set)
        Button btn_set;

        private ViewHolderCustom(View view) {
            ButterKnife.bind(this, view);
        }
    }

    /* Use view holder for Input one value */
    static class ViewHolderInputValue {
        @BindView(R.id.et_value)
        EditText userInput;
        @BindView(R.id.btn_set)
        Button btn_set;

        private ViewHolderInputValue(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
