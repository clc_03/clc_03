package com.dunarctic.timetablemanagement.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.COLOR_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DESCRIPTION_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.END_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.IS_DONE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.NID_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_CHANGE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_CHANGE_REMIDNER;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_DELETE_ITEM;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_NOT_CHANGE;
import static com.dunarctic.timetablemanagement.Utility.Utility.START_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TITLE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TYPE_KEY;

/**
 * Created by dunarctic on 12/29/2017.
 */

public class DetailWorkItemActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_detail)
    Toolbar toolbar_detail;
    @BindView(R.id.txt_title)
    TextView txt_title;
    @BindView(R.id.iv_type)
    ImageView iv_type;
    @BindView(R.id.txt_dateFrom)
    TextView txt_dateFrom;
    @BindView(R.id.txt_dateTo)
    TextView txt_dateTo;
    @BindView(R.id.txt_time)
    TextView txt_time;
    @BindView(R.id.txt_notes)
    TextView txt_notes;
    @BindView(R.id.txt_reminder)
    TextView txt_reminder;
    @BindView(R.id.sw_reminder)
    Switch sw_reminder;
    @BindView(R.id.btn_delete)
    Button btn_delete;

    private WorkItem item;
    private WorkItemDatabaseHelper dbhelper; // util to do update stuffs in db
    private DateTimeUtility dtFormat; // util to do stuffs with notification
    private String reminderStrChanged;
    private String reminderStr;
    private Context context;
    private int mTypeReminder = 0, mReminderValue = 1, mReminderChoice = 2;
    private AlertDialog alert;
    private AlertDialog custom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        ButterKnife.bind(this);
        //create back button on top-left of toolbar
        toolbar_detail.setNavigationIcon(R.drawable.ic_action_back);
        toolbar_detail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        context = DetailWorkItemActivity.this;
        dbhelper = new WorkItemDatabaseHelper(context);
        dtFormat = new DateTimeUtility();
        getDataFromIntent();
        setData();
    }

    @Override
    public void onBackPressed() {
        if (reminderStr.equals(reminderStrChanged))
            setResult(RESULT_NOT_CHANGE);
        else {
            Intent intent = new Intent();
            intent.putExtra(TITLE_KEY, item.getTitleWI());
            intent.putExtra(REMINDER_KEY, reminderStr);
            intent.putExtra(REMINDER_CHANGE_KEY, reminderStrChanged);
            intent.putExtra(NID_KEY, item.getNotificationID());
            intent.putExtra(TYPE_KEY, item.getTypeIdWI());
            intent.putExtra(COLOR_KEY, item.getColorIdWI());
            setResult(RESULT_CHANGE_REMIDNER, intent);
        }
        super.onBackPressed();
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        item = new WorkItem(intent.getStringExtra(TITLE_KEY),
                intent.getExtras().getInt(TYPE_KEY),
                intent.getExtras().getBoolean(IS_DONE_KEY),
                intent.getStringExtra(DESCRIPTION_KEY),
                intent.getStringExtra(START_DATE_KEY),
                intent.getStringExtra(END_DATE_KEY),
                intent.getStringExtra(REMINDER_KEY),
                intent.getExtras().getInt(COLOR_KEY),
                intent.getExtras().getInt(NID_KEY));
    }

    private void setData() {
        // update UI with the content taken from intent
        final String titleStr = item.getTitleWI();
        final int type = item.getTypeIdWI();
        final boolean isDone = item.isDoneWI();
        final String noteStr = item.getDescriptionWI();
        final String startDateStr = item.getStartDateWI();
        final String endDateStr = item.getEndDateWI();
        final int color = item.getColorIdWI();
        reminderStrChanged = item.getReminderWI();
        reminderStr = reminderStrChanged;
        final int notificationID = item.getNotificationID();
        final Calendar calEnd = Calendar.getInstance();
        Date date = dtFormat.StringToDateTime(endDateStr);
        calEnd.setTime(date);

        final Calendar calStart = Calendar.getInstance();
        date = dtFormat.StringToDateTime(startDateStr);
        calStart.setTime(date);

        toolbar_detail.setBackgroundColor(color);
        txt_title.setText(titleStr);
        switch (type) {
            case DEADLINE_TYPE:
                iv_type.setImageDrawable(getDrawable(R.drawable.type_deadline));
                txt_dateFrom.setText("on " + dtFormat.dateToString(calEnd));
                txt_dateTo.setVisibility(View.GONE);
                txt_time.setText(dtFormat.timeToString(calEnd));
                break;
            case CLASS_TYPE:
                iv_type.setImageDrawable(getDrawable(R.drawable.type_class));
                txt_dateFrom.setText("on " + dtFormat.dateToString(calStart));
                txt_dateTo.setVisibility(View.GONE);
                String range = dtFormat.timeToString(calStart)
                        + " ~ " + dtFormat.timeToString(calEnd);
                txt_time.setText(range);
                break;
            case HOLIDAY_TYPE:
                iv_type.setImageDrawable(getDrawable(R.drawable.type_holiday));
                txt_dateFrom.setText("from " + dtFormat.dateToString(calStart));
                txt_dateTo.setText("to " + dtFormat.dateToString(calEnd));
                txt_time.setVisibility(View.GONE);
                break;
        }
        if (!noteStr.isEmpty())
            txt_notes.setText(noteStr);
        else
            txt_notes.setText(getResources().getString(R.string.test_description));

        if (!reminderStrChanged.isEmpty())
            txt_reminder.setText(reminderStrChanged);
        else
            txt_reminder.setText(getResources().getString(R.string.off));

        if (reminderStrChanged.isEmpty())
            sw_reminder.setChecked(false);
        else
            sw_reminder.setChecked(true);
        sw_reminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!sw_reminder.isChecked()) {
                    mTypeReminder = 0;
                    txt_reminder.setText(getResources().getString(R.string.off));
                    reminderStrChanged = "";
                } else {
                    if (isDone) {
                        Toast.makeText(context, "This item had been done", Toast
                                .LENGTH_SHORT).show();
                        sw_reminder.setChecked(false);
                    } else {
                        //>>>>>>>>>>Chọn lại reminder<<<<<<<<<<
                        if (mTypeReminder != 0)
                            return;
                        sw_reminder.setChecked(false);
                        final View promptsView = LayoutInflater.from(context).inflate(R.layout.dialog_set_reminder, null);
                        final ViewHolderReminder viewHolderReminder = new ViewHolderReminder(promptsView);
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        switch (mTypeReminder) {
                            case 0:
                                viewHolderReminder.iv_no_notification.setVisibility(View.VISIBLE);
                                viewHolderReminder.btn_no_notification
                                        .setTextColor(getResources().getColor(R.color.notifyColor));
                                break;
                            case 5:
                                viewHolderReminder.iv_at_due.setVisibility(View.VISIBLE);
                                viewHolderReminder.btn_at_due.setTextColor(getResources()
                                        .getColor(R.color.notifyColor));
                                break;
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                viewHolderReminder.iv_more.setVisibility(View.VISIBLE);
                                viewHolderReminder.btn_more.setTextColor(getResources()
                                        .getColor(R.color.notifyColor));
                                break;
                        }
                        switch (type) {
                            case DEADLINE_TYPE:
                                viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                        at_due));
                                break;
                            case CLASS_TYPE:
                                viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                        at_start_time));
                                break;
                            case HOLIDAY_TYPE:
                                viewHolderReminder.btn_at_due.setText(getResources().getString(R.string.
                                        at_start_date));
                                break;
                        }
                        setMoreOption(viewHolderReminder);
                        viewHolderReminder.btn_no_notification.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTypeReminder = 0;
                                sw_reminder.setChecked(false);
                                alert.dismiss();
                            }
                        });
                        viewHolderReminder.btn_at_due.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTypeReminder = 5;
                                Calendar cal = Calendar.getInstance();
                                switch (type) {
                                    case DEADLINE_TYPE:
                                        cal.set(calEnd.get(Calendar.YEAR),
                                                calEnd.get(Calendar.MONTH),
                                                calEnd.get(Calendar.DAY_OF_MONTH),
                                                calEnd.get(Calendar.HOUR_OF_DAY),
                                                calEnd.get(Calendar.MINUTE));
                                        break;
                                    case CLASS_TYPE:
                                        cal.set(calStart.get(Calendar.YEAR),
                                                calStart.get(Calendar.MONTH),
                                                calStart.get(Calendar.DAY_OF_MONTH),
                                                calStart.get(Calendar.HOUR_OF_DAY),
                                                calStart.get(Calendar.MINUTE));
                                        break;
                                    case HOLIDAY_TYPE:
                                        cal.set(calStart.get(Calendar.YEAR),
                                                calStart.get(Calendar.MONTH),
                                                calStart.get(Calendar.DAY_OF_MONTH),
                                                calStart.get(Calendar.HOUR_OF_DAY),
                                                calStart.get(Calendar.MINUTE));
                                        cal.set(Calendar.HOUR_OF_DAY, 0);
                                        cal.set(Calendar.MINUTE, 0);
                                        break;
                                }
                                reminderStrChanged = dtFormat.dateTimeToString(cal);
                                txt_reminder.setText(reminderStrChanged);
                                sw_reminder.setChecked(true);
                                alert.dismiss();
                            }
                        });
                        viewHolderReminder.btn_more.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mTypeReminder = mReminderChoice; //IMPORTANT
                                Calendar cal = Calendar.getInstance();
                                switch (type) {
                                    case DEADLINE_TYPE:
                                        cal.set(calEnd.get(Calendar.YEAR),
                                                calEnd.get(Calendar.MONTH),
                                                calEnd.get(Calendar.DAY_OF_MONTH),
                                                calEnd.get(Calendar.HOUR_OF_DAY),
                                                calEnd.get(Calendar.MINUTE));
                                        minusCalendar(cal, mTypeReminder, mReminderValue);
                                        break;
                                    case CLASS_TYPE:
                                        cal.set(calStart.get(Calendar.YEAR),
                                                calStart.get(Calendar.MONTH),
                                                calStart.get(Calendar.DAY_OF_MONTH),
                                                calStart.get(Calendar.HOUR_OF_DAY),
                                                calStart.get(Calendar.MINUTE));
                                        minusCalendar(cal, mTypeReminder, mReminderValue);
                                        break;
                                    case HOLIDAY_TYPE:
                                        cal.set(calStart.get(Calendar.YEAR),
                                                calStart.get(Calendar.MONTH),
                                                calStart.get(Calendar.DAY_OF_MONTH),
                                                calStart.get(Calendar.HOUR_OF_DAY),
                                                calStart.get(Calendar.MINUTE));
                                        cal.set(Calendar.HOUR_OF_DAY, 0);
                                        cal.set(Calendar.MINUTE, 0);
                                        minusCalendar(cal, mTypeReminder, mReminderValue);
                                        break;
                                }
                                reminderStrChanged = dtFormat.dateTimeToString(cal);
                                txt_reminder.setText(reminderStrChanged);
                                sw_reminder.setChecked(true);
                                alert.dismiss();
                            }
                        });
                        viewHolderReminder.btn_custom.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final ViewHolderCustom viewHolderCustom;
                                View promptsViewCustom = LayoutInflater.from(context).inflate(R.layout.dialog_custom_notification, null);
                                viewHolderCustom = new ViewHolderCustom(promptsViewCustom);
                                final AlertDialog.Builder builderCustom = new AlertDialog.Builder(context);
                                //turn on specific button and image view
                                switchCustom(viewHolderCustom, mReminderChoice);
                                viewHolderCustom.userInput.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                    }

                                    @Override
                                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                        if (charSequence.toString().isEmpty() || Integer.parseInt
                                                (charSequence.toString()) == 0) {
                                            viewHolderCustom.btn_set.setEnabled(false);
                                        } else {
                                            viewHolderCustom.btn_set.setEnabled(true);
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable editable) {
                                    }
                                });
                                viewHolderCustom.btn_minutes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mReminderChoice = 1;
                                        //turn on specific button and image view
                                        switchCustom(viewHolderCustom, mReminderChoice);
                                    }
                                });
                                viewHolderCustom.btn_hours.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mReminderChoice = 2;
                                        //turn on specific button and image view
                                        switchCustom(viewHolderCustom, mReminderChoice);
                                    }
                                });
                                viewHolderCustom.btn_days.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mReminderChoice = 3;
                                        //turn on specific button and image view
                                        switchCustom(viewHolderCustom, mReminderChoice);
                                    }
                                });
                                viewHolderCustom.btn_weeks.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mReminderChoice = 4;
                                        //turn on specific button and image view
                                        switchCustom(viewHolderCustom, mReminderChoice);
                                    }
                                });
                                viewHolderCustom.btn_set.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mTypeReminder = mReminderChoice;
                                        mReminderValue = Integer.parseInt(viewHolderCustom.userInput.getText().toString());
                                        Calendar cal = Calendar.getInstance();
                                        switch (type) {
                                            case DEADLINE_TYPE:
                                                cal.set(calEnd.get(Calendar.YEAR),
                                                        calEnd.get(Calendar.MONTH),
                                                        calEnd.get(Calendar.DAY_OF_MONTH),
                                                        calEnd.get(Calendar.HOUR_OF_DAY),
                                                        calEnd.get(Calendar.MINUTE));
                                                minusCalendar(cal, mTypeReminder, mReminderValue);
                                                break;
                                            case CLASS_TYPE:
                                                cal.set(calStart.get(Calendar.YEAR),
                                                        calStart.get(Calendar.MONTH),
                                                        calStart.get(Calendar.DAY_OF_MONTH),
                                                        calStart.get(Calendar.HOUR_OF_DAY),
                                                        calStart.get(Calendar.MINUTE));
                                                minusCalendar(cal, mTypeReminder, mReminderValue);
                                                break;
                                            case HOLIDAY_TYPE:
                                                cal.set(calStart.get(Calendar.YEAR),
                                                        calStart.get(Calendar.MONTH),
                                                        calStart.get(Calendar.DAY_OF_MONTH),
                                                        calStart.get(Calendar.HOUR_OF_DAY),
                                                        calStart.get(Calendar.MINUTE));
                                                cal.set(Calendar.HOUR_OF_DAY, 0);
                                                cal.set(Calendar.MINUTE, 0);
                                                minusCalendar(cal, mTypeReminder, mReminderValue);
                                                break;
                                        }
                                        reminderStrChanged = dtFormat.dateTimeToString(cal);
                                        txt_reminder.setText(reminderStrChanged);
                                        sw_reminder.setChecked(true);
                                        custom.dismiss();
                                        alert.dismiss();
                                    }
                                });
                                builderCustom.setView(promptsViewCustom);
                                // set dialog message
                                custom = builderCustom.create();
                                custom.show();
                            }
                        });
                        builder.setView(promptsView);
                        alert = builder.create();
                        alert.show();
                    }
                }
            }
        });
        //if delete is pressed then delete item in databse and alro remove object for
        // notify changed
        btn_delete.setBackgroundColor(color);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<WorkItem> deletedList = new ArrayList<>(); //list of work items
                AlertDialog.Builder alertDialog_confirm = new AlertDialog.Builder(context);
                // set information for dialog
                alertDialog_confirm.setMessage("Are you sure to delete this item?")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // it user click cancel
                                // do nothing, just back the main screen
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent();
                                intent.putExtra(TITLE_KEY, item.getTitleWI());
                                intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                                intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                                intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                                intent.putExtra(REMINDER_KEY, reminderStr);
                                intent.putExtra(COLOR_KEY, item.getColorIdWI());
                                intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                                intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                                intent.putExtra(NID_KEY, item.getNotificationID());
                                setResult(RESULT_DELETE_ITEM, intent);
                                Toast.makeText(context, "Item deleted", Toast.LENGTH_LONG)
                                        .show();
                                finish();
                            }
                        });
                AlertDialog alert = alertDialog_confirm.create();
                alert.show();
            }
        });
    }

    public String getMoreStr() {
        String customStr = mReminderValue + " ";
        switch (mReminderChoice) {
            case 1:
                customStr += getResources().getString(R.string.minutes_before);
                break;
            case 2:
                customStr += getResources().getString(R.string.hours_before);
                break;
            case 3:
                customStr += getResources().getString(R.string.days_before);
                break;
            case 4:
                customStr += getResources().getString(R.string.weeks_before);
                break;
        }
        return customStr;
    }

    public void setMoreOption(ViewHolderReminder viewHolderReminder) {
        String customStr = getMoreStr();
        switch (mReminderChoice) {
            case 1:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 2:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 3:
                viewHolderReminder.btn_more.setText(customStr);
                break;
            case 4:
                viewHolderReminder.btn_more.setText(customStr);
                break;
        }
    }

    /*  type = 1 : apply to MINUTE
        type = 2 : apply to HOUR
        type = 3 : apply to DAY_OF_MONTH
        type = 4 : apply to WEEK_OF_YEAR */
    public void minusCalendar(Calendar calendar, int type, int value) {
        switch (type) {
            case 1:
                calendar.add(Calendar.MINUTE, -value);
                break;
            case 2:
                calendar.add(Calendar.HOUR_OF_DAY, -value);
                break;
            case 3:
                calendar.add(Calendar.DAY_OF_MONTH, -value);
                break;
            case 4:
                calendar.add(Calendar.WEEK_OF_YEAR, -value);
                break;
            default: //not change
                break;
        }
    }

    /*  onId = 1 : only 1st attributes is visible
        onId = 2 : only 2nd attributes is visible
        onId = 3 : only 3rd attributes is visible
        onId = 4 : only 4th attributes is visible */
    public void switchCustom(ViewHolderCustom view, int onId) {
        Resources res = getResources();
        view.iv_minutes.setVisibility((onId == 1) ? View.VISIBLE : View.INVISIBLE);
        view.btn_minutes.setTextColor((onId == 1) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_minutes.setText((onId == 1) ? res.getString(R.string
                .minutes_before) : res.getString(R.string
                .minutes));

        view.iv_hours.setVisibility((onId == 2) ? View.VISIBLE : View.INVISIBLE);
        view.btn_hours.setTextColor((onId == 2) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_hours.setText((onId == 2) ? res.getString(R.string
                .hours_before) : res.getString(R.string
                .hours));

        view.iv_days.setVisibility((onId == 3) ? View.VISIBLE : View.INVISIBLE);
        view.btn_days.setTextColor((onId == 3) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_days.setText((onId == 3) ? res.getString(R.string
                .days_before) : res.getString(R.string
                .days));

        view.iv_weeks.setVisibility((onId == 4) ? View.VISIBLE : View.INVISIBLE);
        view.btn_weeks.setTextColor((onId == 4) ? res.getColor(R.color.notifyColor) : res
                .getColor(R.color.textColorDark));
        view.btn_weeks.setText((onId == 4) ? res.getString(R.string
                .weeks_before) : res.getString(R.string
                .weeks));
    }

    /* Use view holder for Reminder */
    static class ViewHolderReminder {
        @BindView(R.id.iv_no_notification)
        ImageView iv_no_notification;
        @BindView(R.id.btn_no_notification)
        Button btn_no_notification;
        @BindView(R.id.iv_at_due)
        ImageView iv_at_due;
        @BindView(R.id.btn_at_due)
        Button btn_at_due;
        @BindView(R.id.iv_more)
        ImageView iv_more;
        @BindView(R.id.btn_more)
        Button btn_more;
        @BindView(R.id.btn_custom)
        Button btn_custom;

        private ViewHolderReminder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    /* Use view holder for Custom reminder */
    static class ViewHolderCustom {
        @BindView(R.id.iv_minutes)
        ImageView iv_minutes;
        @BindView(R.id.btn_minutes)
        Button btn_minutes;
        @BindView(R.id.iv_hours)
        ImageView iv_hours;
        @BindView(R.id.btn_hours)
        Button btn_hours;
        @BindView(R.id.iv_days)
        ImageView iv_days;
        @BindView(R.id.btn_days)
        Button btn_days;
        @BindView(R.id.iv_weeks)
        ImageView iv_weeks;
        @BindView(R.id.btn_weeks)
        Button btn_weeks;
        @BindView(R.id.et_value)
        EditText userInput;
        @BindView(R.id.btn_set)
        Button btn_set;

        private ViewHolderCustom(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
