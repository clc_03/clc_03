package com.dunarctic.timetablemanagement.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    public static final int SIGNIN_TYPE = 0;
    public static final int REGISTRATION_TYPE = 1;
    @BindView(R.id.txt_email)
    EditText email;
    @BindView(R.id.txt_password)
    EditText password;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnRegis)
    Button btnRegis;
    @BindView(R.id.radio_group_type_signin)
    RadioRealButtonGroup radio_group_signin;
    @BindView(R.id.layout_sign_in)
    LinearLayout layout_sign_in;
    @BindView(R.id.layout_registration)
    LinearLayout layout_regis;
    //3: Sign In -- 4:Registration
    private int curType = SIGNIN_TYPE;
    private FirebaseAuth firebaseAuth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //layout_sign_in.setVisibility(View.GONE);
        layout_regis.setVisibility(View.GONE);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        //xtYourName.setText(user.getEmail());

        if (firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
        }

        radio_group_signin.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {

                curType = currentPosition;
                //Toast.makeText(LoginActivity.this,currentPosition+" ",Toast.LENGTH_LONG).show();
                switch (curType) {
                    case SIGNIN_TYPE:
                        //Toast.makeText(LoginActivity.this,"Login",Toast.LENGTH_LONG).show();
                        layout_sign_in.animate().alpha(1.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_sign_in.setVisibility(View.VISIBLE);
                                    }
                                });
                        layout_regis.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_regis.setVisibility(View.GONE);
                                    }
                                });

                        break;
                    case REGISTRATION_TYPE:
                        //Toast.makeText(LoginActivity.this,"REGIS",Toast.LENGTH_LONG).show();
                        layout_sign_in.animate().alpha(0.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_sign_in.setVisibility(View.GONE);
                                    }
                                });
                        layout_regis.animate().alpha(1.0f).setDuration(300)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layout_regis.setVisibility(View.VISIBLE);
                                    }
                                });

                        break;
                }
                resetValues();
            }

        });


        btnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String Email = email.getText().toString();
                String Password = password.getText().toString();
                if (TextUtils.isEmpty(Email)) {
                    Toast.makeText(LoginActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(Password)) {
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
                    return;
                }
                (firebaseAuth.signInWithEmailAndPassword(Email, Password))
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LoginActivity.this, "Login successfully", Toast.LENGTH_LONG).show();
                                    finish();
                                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                                } else {
                                    Log.e("ERROR", task.getException().toString());
                                    Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }

        });
        btnRegis.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String Email = email.getText().toString();
                String Password = password.getText().toString();
                if (TextUtils.isEmpty(Email)) {
                    Toast.makeText(LoginActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(Password)) {
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_LONG).show();
                    return;
                }
                (firebaseAuth.createUserWithEmailAndPassword(Email, Password))
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LoginActivity.this, "Registration successful", Toast.LENGTH_LONG).show();
                                    finish();
                                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                                } else {
                                    Log.e("ERROR", task.getException().toString());
                                    Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }

    public void resetValues() {
        //reset interface of all items in changeable layout
        btnSignIn.setText("Sign in");
        btnRegis.setText("Registration");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}

