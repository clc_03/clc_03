package com.dunarctic.timetablemanagement.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Adapter.TimelineAdapter;
import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.Utility;
import com.dunarctic.timetablemanagement.View.CustomViewPager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Fragment.DayFragment.listWorkItems;
import static com.dunarctic.timetablemanagement.Fragment.MonthFragment.listMonthWorkItems;
import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HIDE_TABLAYOUT_FAB;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.PASSWORD_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REFRESH_CODE;
import static com.dunarctic.timetablemanagement.Utility.Utility.SHOW_TABLAYOUT_FAB;

public class MainActivity extends AppCompatActivity implements MainCallBacks {
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav)
    NavigationView nvDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.viewPager)
    CustomViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    DateTimeUtility dtFormat;
    private AddWorkItemTask AddTask = null;
    private DeleteWorkItemTask deleteTask = null;
    private int idPager = 0;
    private int idNav = R.id.nav_overview;
    private String curPass;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //create the activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initializeUI();
    }

    //initialize view pager and navigation bar
    private void initializeUI() {
        dtFormat = new DateTimeUtility();
        //handle toolbar
        setSupportActionBar(toolbar);

        // create the drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        viewPager.setAdapter(new TimelineAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                //update data in this page according to position
                idPager = position;
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AddTask = new AddWorkItemTask();
                        AddTask.execute();
                    }
                }, 300);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //display current date on navigation bar and toolbar
        Calendar cal = Calendar.getInstance();
        String dateString = dtFormat.shortDateFormat(cal);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(dateString);
        appbar.setBackground(Utility.getTodayBackground(MainActivity.this));

        //initialize floating action button
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AddWIIntent = new Intent(MainActivity.this, AddWIActivity.class);
                startActivityForResult(AddWIIntent, REFRESH_CODE);
            }
        });

        nvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks
                selectDrawerItem(item);
                return true;
            }
        });
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        View header = nvDrawer.getHeaderView(0);
        TextView name = header.findViewById(R.id.your_name);
        if (firebaseAuth.getCurrentUser() != null) {
            name.setText(user.getEmail());
        }

        AddTask = new AddWorkItemTask();
        AddTask.execute();
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_CODE) {
            if (resultCode == RESULT_OK) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AddTask = new AddWorkItemTask();
                        AddTask.execute();
                    }
                }, 300);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu to add items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_search:
                Intent toSearchingActivity = new Intent(MainActivity.this, SearchingActivity.class);
                startActivityForResult(toSearchingActivity, REFRESH_CODE);
                return true;
            case R.id.action_remove_done:
                AlertDialog.Builder alertDialog_confirm = new AlertDialog.Builder(MainActivity.this);
                // set information for dialog
                alertDialog_confirm.setMessage("Are you sure to delete all done item?")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // it user click cancel
                                // do nothing, just back the main screen
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //if has password protection
                                if (PreferenceManager
                                        .getDefaultSharedPreferences(getApplicationContext())
                                        .getBoolean("security_del", false)) {
                                    LayoutInflater li = LayoutInflater.from(MainActivity.this);
                                    final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                    // set prompts.xml to alert dialog builder
                                    alertDialogBuilder.setView(promptsView);

                                    final EditText userInput = (EditText) promptsView
                                            .findViewById(R.id.password_et);

                                    // set dialog message
                                    alertDialogBuilder.setCancelable(false)
                                            .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    // get user input and check it
                                                    String curPass = PreferenceManager
                                                            .getDefaultSharedPreferences
                                                                    (getApplicationContext())
                                                            .getString(PASSWORD_KEY, "");
                                                    curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                                                    if (userInput.getText().toString().equals(curPass)) {
                                                        ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                                        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(MainActivity.this);
                                                        mDbHelper.removeDone(MainActivity.this);
                                                        viewPager.getAdapter().notifyDataSetChanged();
                                                    } else {
                                                        Toast.makeText(MainActivity.this, "Wrong " +
                                                                "password", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }).setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                    alertDialogBuilder.show();
                                } else {
                                    WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(MainActivity.this);
                                    mDbHelper.removeDone(MainActivity.this);
                                    viewPager.getAdapter().notifyDataSetChanged();
                                }
                            }
                        });
                AlertDialog alert = alertDialog_confirm.create();
                alert.show();
                return true;


            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //handle item in navigation view menu clicks
    public void selectDrawerItem(MenuItem item) {
        // Handle navigation view item clicks
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_account:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_overview:
            case R.id.nav_deadline:
            case R.id.nav_class:
            case R.id.nav_holiday:
                idNav = id;
                AddTask = new AddWorkItemTask();
                AddTask.execute();
                break;
            case R.id.nav_reminder:
                Intent toReminderActivity = new Intent(MainActivity.this, ReminderActivity.class);
                startActivityForResult(toReminderActivity, REFRESH_CODE);
                break;
            case R.id.nav_backup:
                Toast.makeText(MainActivity.this, "Handle Backup item", Toast.LENGTH_LONG).show();
                break;
            case R.id.nav_settings:
                final Context context = this;
                // get user input and check it
                curPass = PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getString(PASSWORD_KEY, "");
                curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                //if has password protection
                if (!curPass.isEmpty()) {
                    LayoutInflater li = LayoutInflater.from(context);
                    final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set prompts.xml to alert dialog builder
                    alertDialogBuilder.setView(promptsView);
                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.password_et);
                    // set dialog message
                    alertDialogBuilder.setCancelable(false).setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (userInput.getText().toString().equals(curPass)) {
                                ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                Intent i = new Intent(context, SettingsActivity.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(context, "Wrong " +
                                        "password", Toast.LENGTH_LONG).show();
                            }
                        }
                    }).setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alertDialogBuilder.show();
                } else {
                    Intent i = new Intent(context, SettingsActivity.class);
                    startActivity(i);
                }
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onDestroy() {
        if (AddTask != null) {
            AddTask.cancel(false);
        }
        super.onDestroy();
    }

    @Override
    public void onMsgFromFragmentToMain(String message) {
        if (message.equals(HIDE_TABLAYOUT_FAB)) {
            tabLayout.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            viewPager.setPagingEnabled(false);
        } else if (message.equals(SHOW_TABLAYOUT_FAB)) {
            tabLayout.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            viewPager.setPagingEnabled(true);
        }
    }

    @Override
    public void removeListItem(List<WorkItem> list) {
        if (!list.isEmpty()) {
            deleteTask = new DeleteWorkItemTask(MainActivity.this, list);
            deleteTask.execute();
        }
    }

    private class AddWorkItemTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listWorkItems == null) {
                listWorkItems = new ArrayList<>();
            } else {
                listWorkItems.clear();
            }

            if (listMonthWorkItems == null) {
                listMonthWorkItems = new ArrayList<>();
            } else {
                listMonthWorkItems.clear();
            }
        }

        @Override
        protected Void doInBackground(Void... unused) {
            WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(MainActivity.this);
            mDbHelper.updateDone(MainActivity.this);
            mDbHelper.updateReminder(MainActivity.this);
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            Cursor cursor;
            Calendar current = Calendar.getInstance();
            String currentDateTime = dtFormat.dateTimeToString(current);
            String typeFilter;
            switch (idNav) {
                case R.id.nav_deadline:
                    typeFilter = " AND "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE
                            + " = "
                            + DEADLINE_TYPE;
                    break;
                case R.id.nav_class:
                    typeFilter = " AND "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE
                            + " = "
                            + CLASS_TYPE;
                    break;
                case R.id.nav_holiday:
                    typeFilter = " AND "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE
                            + " = "
                            + HOLIDAY_TYPE;
                    break;
                default:
                    typeFilter = "";
                    break;
            }
            switch (idPager) {
                case 0: //retrieve data having due date is today
                    cursor = db.rawQuery(
                            "Select "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION
                                    + " FROM "
                                    + WorkItemDatabaseHelper.WorkItemTable.TABLE_NAME
                                    + " WHERE strftime('%Y-%m-%d', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE
                                    + " ) <= strftime('%Y-%m-%d','"
                                    + currentDateTime
                                    + "') AND strftime('%Y-%m-%d', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE
                                    + ") >= strftime('%Y-%m-%d','"
                                    + currentDateTime
                                    + "')"
                                    + typeFilter
                            , null);
                    break;
                case 1: //retrieve data having due date in this week
                    cursor = db.rawQuery(
                            "Select "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION
                                    + " FROM "
                                    + WorkItemDatabaseHelper.WorkItemTable.TABLE_NAME
                                    + " WHERE ((strftime('%W', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE
                                    + ") <= strftime('%W','"
                                    + currentDateTime
                                    + "') AND strftime('%W', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE
                                    + ") >= strftime('%W','"
                                    + currentDateTime
                                    + "') AND strftime('%Y', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE
                                    + ") = strftime('%Y', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE
                                    + ")) OR (strftime('%Y', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE
                                    + ") < strftime('%Y', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE
                                    + ") AND (strftime('%W', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE
                                    + ") >= strftime('%W','"
                                    + currentDateTime
                                    + "') OR strftime('%W', "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE
                                    + ") <= strftime('%W','"
                                    + currentDateTime
                                    + "'))))"
                                    + typeFilter
                            , null);
                    break;
                case 2: //retrieve data having due date in this month
                    cursor = db.rawQuery(
                            "Select "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION + ", "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION
                                    + " FROM "
                                    + WorkItemDatabaseHelper.WorkItemTable.TABLE_NAME
                                    + " WHERE "
                                    + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE
                                    + " = 0"
                            , null);
                    break;
                default:
                    cursor = null;
            }
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String title = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE));
                        if (title == null)
                            break;
                        int type = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE));
                        String startDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE));
                        String endDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE));
                        boolean isDone = "1".equals(cursor.getString(
                                cursor.getColumnIndex(
                                        WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE)));
                        String reminderDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER));
                        int colorID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID));
                        String description = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION));
                        int notificationID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION));

                        WorkItem cellItem = new WorkItem(title, type, isDone, description, startDate, endDate,
                                reminderDate, colorID, notificationID);
                        if (!listWorkItems.contains(cellItem)) {
                            listWorkItems.add(cellItem);
                            listMonthWorkItems.add(cellItem);
                        }

                        //for sorting
                        startDate = dtFormat.formatTime(startDate);
                        WorkItem headerItem = new WorkItem(startDate);
                        if (!listWorkItems.contains(headerItem)) {
                            listWorkItems.add(headerItem);
                        }
                    } while (cursor.moveToNext());
                }
                Collections.sort(listWorkItems);
                Collections.sort(listMonthWorkItems);
                cursor.moveToFirst();
                cursor.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            viewPager.getAdapter().notifyDataSetChanged();
            AddTask = null;
        }
    }

    private class DeleteWorkItemTask extends AsyncTask<Void, Void, Void> {
        private List<WorkItem> mList;
        private Context mContext;

        DeleteWorkItemTask(Context context, List<WorkItem> list) {
            this.mList = list;
            this.mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(MainActivity.this);
            for (int i = (mList.size() - 1); i >= 0; i--) {
                mDbHelper.removeItem(mList.get(i), mContext);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            viewPager.getAdapter().notifyDataSetChanged();
            AddTask = null;
        }
    }
}
