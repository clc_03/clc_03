package com.dunarctic.timetablemanagement.Activity;

import com.dunarctic.timetablemanagement.Database.WorkItem;

import java.util.List;

/**
 * Created by dunarctic on 27.12.2017
 */

interface MainCallBacks {
    void onMsgFromFragmentToMain(String message);

    void removeListItem(List<WorkItem> list);
}
