package com.dunarctic.timetablemanagement.Activity;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.dunarctic.timetablemanagement.Adapter.ReminderAdapter;
import com.dunarctic.timetablemanagement.Database.Reminder;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.Utility;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReminderActivity extends AppCompatActivity {
    private static ArrayList<Reminder> listReminder;
    ReminderAdapter adapter;
    @BindView(R.id.toolbarReminder)
    Toolbar toolbarReminder;
    @BindView(R.id.lvReminder)
    ListView lvReminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        ButterKnife.bind(this);
        setSupportActionBar(toolbarReminder);
        //create back button on top-left of toolbar
        toolbarReminder.setNavigationIcon(R.drawable.ic_action_back);
        toolbarReminder.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbarReminder.setBackground(Utility.getTodayBackground(ReminderActivity.this));

        if (listReminder == null)
            listReminder = new ArrayList<>();
        else
            listReminder.clear();

        adapter = new ReminderAdapter(this, R.layout.reminder_item, listReminder);
        lvReminder.setAdapter(adapter);
        AddReminderTask addReminderTask = new AddReminderTask();
        addReminderTask.execute();
        setResult(Activity.RESULT_OK);
    }

    private class AddReminderTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(ReminderActivity.this);
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            Cursor cursor;
            cursor = db.rawQuery(
                    "Select "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION
                            + " FROM "
                            + WorkItemDatabaseHelper.WorkItemTable.TABLE_NAME
                            + " WHERE "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE
                            + " = "
                            + 0
                            + " AND "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER
                            + " != ''"
                    , null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String title = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE));
                        if (title == null)
                            break;
                        String reminderDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER));
                        int colorID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID));
                        int type = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE));
                        int nID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION));

                        Reminder cellItem = new Reminder(title, reminderDate, colorID, nID, type);
                        if (!listReminder.contains(cellItem)) {
                            listReminder.add(cellItem);
                        }

                    } while (cursor.moveToNext());
                }
                Collections.sort(listReminder);
                cursor.moveToFirst();
                cursor.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            adapter.notifyDataSetChanged();
        }
    }
}
