package com.dunarctic.timetablemanagement.Activity;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Adapter.SearchingAdapter;
import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;
import com.dunarctic.timetablemanagement.Utility.Utility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Adapter.SearchingAdapter.listSearching;
import static com.dunarctic.timetablemanagement.Utility.Utility.COLOR_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.DESCRIPTION_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.END_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.IS_DONE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.NID_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.PASSWORD_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REFRESH_CODE;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_CHANGE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_CHANGE_REMIDNER;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_NOT_CHANGE;
import static com.dunarctic.timetablemanagement.Utility.Utility.START_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TITLE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TYPE_KEY;

public class SearchingActivity extends AppCompatActivity {
    SearchingAdapter adapter;
    String input = "";
    DateTimeUtility dtFormat;
    @BindView(R.id.txtSearch)
    EditText txtsearch;
    @BindView(R.id.listview_search)
    ListView listSearch;
    private AddSearchingTask addSearchingTask = null;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searching);
        ButterKnife.bind(this);
        mContext = SearchingActivity.this;
        txtsearch.setBackground(Utility.getTodayBackground(mContext));

        if (listSearching == null) {
            listSearching = new ArrayList<>();
        } else {
            listSearching.clear();
        }

        adapter = new SearchingAdapter(this, listSearching);
        listSearch.setAdapter(adapter);
        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int curPosition = position;
                //if has password protection
                if (PreferenceManager
                        .getDefaultSharedPreferences(mContext)
                        .getBoolean("security_del", false)) {
                    LayoutInflater li = LayoutInflater.from(mContext);
                    final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                    // set prompts.xml to alert dialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.password_et);

                    // set dialog message
                    alertDialogBuilder.setCancelable(false)
                            .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and check it
                                    String curPass = PreferenceManager
                                            .getDefaultSharedPreferences(mContext)
                                            .getString(PASSWORD_KEY, "");
                                    curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                                    if (userInput.getText().toString().equals(curPass)) {
                                        ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                        Intent intent = new Intent(mContext, DetailWorkItemActivity.class);
                                        WorkItem item = listSearching.get(curPosition);
                                        intent.putExtra(TITLE_KEY, item.getTitleWI());
                                        intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                                        intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                                        intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                                        intent.putExtra(REMINDER_KEY, item.getReminderWI());
                                        intent.putExtra(COLOR_KEY, item.getColorIdWI());
                                        intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                                        intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                                        intent.putExtra(NID_KEY, item.getNotificationID());
                                        startActivityForResult(intent, REFRESH_CODE);
                                    } else {
                                        Toast.makeText(mContext, "Wrong " +
                                                "password", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }).setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alertDialogBuilder.show();
                } else {
                    Intent intent = new Intent(mContext, DetailWorkItemActivity.class);
                    WorkItem item = listSearching.get(curPosition);
                    intent.putExtra(TITLE_KEY, item.getTitleWI());
                    intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                    intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                    intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                    intent.putExtra(REMINDER_KEY, item.getReminderWI());
                    intent.putExtra(COLOR_KEY, item.getColorIdWI());
                    intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                    intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                    intent.putExtra(NID_KEY, item.getNotificationID());
                    startActivityForResult(intent, REFRESH_CODE);
                }
            }
        });
        txtsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().equals("")) {
                    listSearching.clear();
                } else {
                    input = charSequence.toString();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            addSearchingTask = new AddSearchingTask();
                            addSearchingTask.execute();
                        }
                    }, 300);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_CODE) {
            switch (resultCode) {
                case RESULT_CHANGE_REMIDNER:
                    ReminderUtility reminderUti = new ReminderUtility(mContext);
                    DateTimeUtility dtFormat = new DateTimeUtility();
                    WorkItemDatabaseHelper dbhelper = new WorkItemDatabaseHelper(mContext);

                    String titleStr = data.getStringExtra(TITLE_KEY);
                    String reminderChange = data.getStringExtra(REMINDER_CHANGE_KEY);
                    String reminderStr = data.getStringExtra(REMINDER_KEY);
                    int notificationID = data.getExtras().getInt(NID_KEY);
                    int type = data.getExtras().getInt(TYPE_KEY);
                    int color = data.getExtras().getInt(COLOR_KEY);

                    if (reminderChange.isEmpty()) {
                        //cancel notification
                        Calendar reminderCal = Calendar.getInstance();
                        reminderCal.setTime(dtFormat.StringToDateTime(reminderStr));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderCal, type, color);
                        reminderUti.cancelNotification(set, notificationID);
                        dbhelper.removeReminder(notificationID, mContext);
                    } else {
                        //update notification
                        Calendar reminderChangeCal = Calendar.getInstance();
                        reminderChangeCal.setTime(dtFormat.StringToDateTime(reminderChange));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderChangeCal, type, color);
                        reminderUti.setNotification(set, notificationID, reminderChangeCal);
                        dbhelper.changeReminder(notificationID, reminderChange, mContext);
                    }

                    for (WorkItem item : listSearching) {
                        //if there are any checked day
                        if (item.getNotificationID() == notificationID)
                            item.setReminderWI(reminderChange);
                    }

                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_NOT_CHANGE:

                    break;
            }
        }
    }

    private class AddSearchingTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (listSearching == null)
                listSearching = new ArrayList<>();
            else
                listSearching.clear();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
            SQLiteDatabase db = mDbHelper.getReadableDatabase();
            final Cursor cursor;

            cursor = db.rawQuery(
                    "Select "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION + ", "
                            + WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION
                            + " FROM "
                            + WorkItemDatabaseHelper.WorkItemTable.TABLE_NAME
                    , null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String title = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TITLE));
                        if (title == null)
                            break;
                        int type = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_TYPE));
                        String startDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_START_DATE));
                        String endDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_END_DATE));
                        boolean isDone = "1".equals(cursor.getString(
                                cursor.getColumnIndex(
                                        WorkItemDatabaseHelper.WorkItemTable.COLUMN_IS_DONE)));
                        String reminderDate = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_REMINDER));
                        int colorID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_COLOR_ID));
                        String description = cursor.getString(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_DESCRIPTION));
                        int notificationID = cursor.getInt(cursor.getColumnIndex(
                                WorkItemDatabaseHelper.WorkItemTable.COLUMN_NOTIFICATION));

                        String lowerInput = input.toLowerCase();
                        String lowerCurrent = title.toLowerCase();
                        if (lowerInput.equals(lowerCurrent)) {
                            WorkItem cellItem = new WorkItem(title, type, isDone, description,
                                    startDate, endDate, reminderDate, colorID, notificationID);
                            if (!listSearching.contains(cellItem)) {
                                listSearching.add(cellItem);
                            }

                        }
                    } while (cursor.moveToNext());
                }
                Collections.sort(listSearching);
                cursor.moveToFirst();
                cursor.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            super.onPostExecute(unused);
            adapter.notifyDataSetChanged();
        }

    }
}
