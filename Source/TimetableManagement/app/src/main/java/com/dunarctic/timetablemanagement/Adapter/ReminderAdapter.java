package com.dunarctic.timetablemanagement.Adapter;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Database.Reminder;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TrungNhan on 27.12.2017
 */

public class ReminderAdapter extends BaseAdapter {
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private Context context;
    private LayoutInflater inflater;
    private int layout;
    private ArrayList<Reminder> listReminder;
    private int mColor;

    public ReminderAdapter(Context context, int layout, ArrayList<Reminder> listReminder) {
        this.context = context;
        this.layout = layout;
        this.listReminder = listReminder;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listReminder.size();
    }

    @Override
    public Object getItem(int i) {
        return listReminder.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(layout, null);
            //anh xa view
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (holder == null) {
            return view;
        }
        //gan gia tri
        final Reminder reminder = listReminder.get(position);
        final String mTitle = reminder.getTitle();
        final String mReminder = reminder.getDateTime();
        mColor = reminder.getColorID();
        final int mNotificationID = reminder.getNotificationID();
        final int mType = reminder.getType();
        holder.txvTime.setText(reminder.getTime());
        holder.txvDate.setText(reminder.getDate());
        holder.txvName.setText(mTitle);
        holder.txvName.setTextColor(mColor);

        byte factor = 31;// 0-255
        mColor = (factor << 24) | (mColor & 0x00ffffff);
        holder.txvTime.setBackgroundColor(mColor);
        holder.swOnOff.setChecked(true);
        holder.swOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!holder.swOnOff.isChecked()) {
                    //>>>>>>>>Cancel Notification<<<<<<<<
                    ReminderUtility reminderUti = new ReminderUtility(context);
                    DateTimeUtility dtFormat = new DateTimeUtility();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dtFormat.StringToDateTime(mReminder));
                    Notification set =
                            reminderUti.getNotification(mTitle, calendar, mType, mColor);
                    reminderUti.cancelNotification(set, mNotificationID);

                    WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(context);
                    mDbHelper.removeReminder(mNotificationID, context);
                    listReminder.remove(reminder);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    }, 1000);
                    Toast.makeText(context, mTitle + " have been turned off", Toast
                            .LENGTH_LONG).show();
                }
                {
                    //switch button sau khi bị tắt thì item đó sẽ bị xóa, và cập nhật dữ liệu
                    // trong database, nên không cần xử lý else
                }
            }
        });
        return view;
    }

    static class ViewHolder {
        @BindView(R.id.txvTime)
        TextView txvTime;
        @BindView(R.id.txvDate)
        TextView txvDate;
        @BindView(R.id.txvName)
        TextView txvName;
        @BindView(R.id.swOnOff)
        Switch swOnOff;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

