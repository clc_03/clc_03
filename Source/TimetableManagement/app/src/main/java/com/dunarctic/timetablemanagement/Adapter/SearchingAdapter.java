package com.dunarctic.timetablemanagement.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;

/**
 * Created by Dinosaur on 28.12.2017
 */

public class SearchingAdapter extends ArrayAdapter<WorkItem> {
    public static List<WorkItem> listSearching;
    private Context mContext;
    private DateTimeUtility dtFormat;
    private LayoutInflater vi;

    public SearchingAdapter(@NonNull Context context, @NonNull List<WorkItem> listSearching) {
        super(context, 0, listSearching);
        dtFormat = new DateTimeUtility();
        this.mContext = context;
        this.listSearching = listSearching;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listSearching.size();
    }

    @Override
    public WorkItem getItem(int i) {
        return listSearching.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        WorkItem itemSearch = (WorkItem) getItem(i);
        ViewHolder viewHolderCell;
        if (view == null) {
            view = vi.inflate(R.layout.workitem, parent, false);
            viewHolderCell = new ViewHolder(view);
            view.setTag(R.id.workitem_key, viewHolderCell);
        } else {
            viewHolderCell = (ViewHolder) view.getTag(R.id.workitem_key);
        }

        if (viewHolderCell != null && itemSearch != null) {
            int color;
            byte factor = 31;// 0-255
            viewHolderCell.wi_title.setText(itemSearch.getTitleWI());
            if (itemSearch.isDoneWI()) {
                color = mContext.getResources().getColor(R.color.time_wi);
                viewHolderCell.iv_color.setBackgroundColor(color);
                viewHolderCell.wi_title.setTextColor(color);
                color = (factor << 24) | (color & 0x00ffffff);
                viewHolderCell.iv_selected.setBackgroundColor(color);
            } else {
                viewHolderCell.wi_title.setTextColor(
                        mContext.getResources().getColor(R.color.name_wi));
                color = itemSearch.getColorIdWI();
                viewHolderCell.iv_color.setBackgroundColor(color);
                viewHolderCell.iv_selected.setBackgroundColor(
                        mContext.getResources().getColor(R.color.cardview_light_background));
            }
            color = (factor << 24) | (color & 0x00ffffff);
            viewHolderCell.iv_type.setBackgroundColor(color);
            String tempDate = "";
            switch (itemSearch.getTypeIdWI()) {
                case DEADLINE_TYPE:
                    tempDate = itemSearch.getEndDateWI();
                    viewHolderCell.iv_type.setImageDrawable(
                            mContext.getDrawable(R.drawable.type_deadline));
                    break;
                case CLASS_TYPE:
                    tempDate = dtFormat.getDateFromString(itemSearch.getStartDateWI())
                            + ": " + dtFormat.getTimeFromString(itemSearch.getStartDateWI())
                            + " ~ " + dtFormat.getTimeFromString(itemSearch.getEndDateWI());
                    viewHolderCell.iv_type.setImageDrawable(
                            mContext.getDrawable(R.drawable.type_class));
                    break;
                case HOLIDAY_TYPE:
                    tempDate = dtFormat.getDateFromString(itemSearch.getStartDateWI())
                            + " ~ " + dtFormat.getDateFromString(itemSearch.getEndDateWI());
                    viewHolderCell.iv_type.setImageDrawable(
                            mContext.getDrawable(R.drawable.type_holiday));
                    break;
            }
            viewHolderCell.wi_time.setText(tempDate);
            viewHolderCell.iv_reminder.setVisibility(
                    (itemSearch.getReminderWI().equals("") || itemSearch.isDoneWI()) ? View
                            .INVISIBLE : View.VISIBLE);
        }
        return view;
    }

    /* Use view holder for later cache in list view */
    static class ViewHolder {
        @BindView(R.id.iv_color)
        ImageView iv_color;
        @BindView(R.id.wi_title)
        TextView wi_title;
        @BindView(R.id.wi_time)
        TextView wi_time;
        @BindView(R.id.iv_type)
        ImageView iv_type;
        @BindView(R.id.iv_reminder)
        ImageView iv_reminder;
        @BindView(R.id.iv_selected)
        LinearLayout iv_selected;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
