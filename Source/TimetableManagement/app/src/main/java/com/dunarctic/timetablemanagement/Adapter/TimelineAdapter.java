package com.dunarctic.timetablemanagement.Adapter;

/**
 * Created by dunarctic on 04.12.2017
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dunarctic.timetablemanagement.Fragment.DayFragment;
import com.dunarctic.timetablemanagement.Fragment.MonthFragment;

public class TimelineAdapter extends FragmentPagerAdapter {
    // list names of three main tabs
    private final String[] listTab = {"TODAY", "WEEK", "MONTH"};

    public TimelineAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 2) {
            return MonthFragment.newInstance(listTab[position]);
        } else {
            return (DayFragment.newInstance(listTab[position]));
        }
    }

    @Override
    public int getCount() {
        return listTab.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listTab[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;//POSITION_NONE causing full view recreation
    }
}
