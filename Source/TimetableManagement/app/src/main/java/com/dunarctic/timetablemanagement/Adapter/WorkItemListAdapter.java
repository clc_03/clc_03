package com.dunarctic.timetablemanagement.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;

/**
 * Created by dunarctic on 06.12.2017
 */

public class WorkItemListAdapter extends ArrayAdapter<WorkItem> {
    private final static int _HEADER = 0;
    private final static int _ITEM = 1;
    private List<WorkItem> WorkItemList;
    private LayoutInflater vi;
    private Context mContext;
    private DateTimeUtility dtFormat;
    private SparseBooleanArray mSelectedItemsIds;

    public WorkItemListAdapter(@NonNull Context context, @NonNull List<WorkItem> objects) {
        super(context, 0, objects);
        mSelectedItemsIds = new SparseBooleanArray();
        this.mContext = context;
        this.WorkItemList = objects;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dtFormat = new DateTimeUtility();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        WorkItem workItem = getItem(position);
        if (workItem != null) {
            if (workItem.isHeader())
                return _HEADER;
        }
        return _ITEM;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        WorkItem workItem = getItem(position);
        if (this.getItemViewType(position) == _HEADER) {
            ViewHolderHeader viewHolderHeader;
            if (view == null) {
                view = vi.inflate(R.layout.time_header, parent, false);
                viewHolderHeader = new ViewHolderHeader(view);
                view.setTag(R.id.header_key, viewHolderHeader);
            } else {
                viewHolderHeader = (ViewHolderHeader) view.getTag(R.id.header_key);
            }
            view.setEnabled(false);
            view.setOnClickListener(null);
            if (viewHolderHeader != null && workItem != null) {
                String str = workItem.getStartDateWI();
                Calendar cal = Calendar.getInstance();
                Date date = dtFormat.StringToDateTime(str);
                cal.setTime(date);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                str = dtFormat.dateToString(cal);
                viewHolderHeader.label.setText(str);
            }
        } else {
            ViewHolderCell viewHolderCell;
            if (view == null) {
                view = vi.inflate(R.layout.workitem, parent, false);
                viewHolderCell = new ViewHolderCell(view);
                view.setTag(R.id.workitem_key, viewHolderCell);
            } else {
                viewHolderCell = (ViewHolderCell) view.getTag(R.id.workitem_key);
            }

            if (viewHolderCell != null && workItem != null) {
                String timeStr;
                int color;
                byte factor = 31;// 0-255
                viewHolderCell.wi_title.setText(workItem.getTitleWI());
                if (workItem.isDoneWI()) {
                    color = mContext.getResources().getColor(R.color.time_wi);
                    viewHolderCell.iv_color.setBackgroundColor(color);
                    viewHolderCell.wi_title.setTextColor(color);
                    color = (factor << 24) | (color & 0x00ffffff);
                    viewHolderCell.iv_selected.setBackgroundColor(color);
                } else {
                    viewHolderCell.wi_title.setTextColor(
                            mContext.getResources().getColor(R.color.name_wi));
                    color = workItem.getColorIdWI();
                    viewHolderCell.iv_color.setBackgroundColor(color);
                    viewHolderCell.iv_selected.setBackgroundColor(
                            mContext.getResources().getColor(R.color.cardview_light_background));
                }
                color = (factor << 24) | (color & 0x00ffffff);
                viewHolderCell.iv_type.setBackgroundColor(color);
                switch (workItem.getTypeIdWI()) {
                    case DEADLINE_TYPE:
                        timeStr = workItem.getEndDateWI();
                        viewHolderCell.wi_time.setText(dtFormat.getTimeFromString(timeStr));
                        viewHolderCell.iv_type.setImageDrawable(
                                getContext().getDrawable(R.drawable.type_deadline));
                        break;
                    case CLASS_TYPE:
                        timeStr = dtFormat.getTimeFromString(workItem.getStartDateWI());
                        timeStr += " ~ " + dtFormat.getTimeFromString(workItem.getEndDateWI());
                        viewHolderCell.wi_time.setText(timeStr);
                        viewHolderCell.iv_type.setImageDrawable(
                                getContext().getDrawable(R.drawable.type_class));
                        break;
                    case HOLIDAY_TYPE:
                        timeStr = dtFormat.getDateFromString(workItem.getStartDateWI());
                        timeStr += " ~ " + dtFormat.getDateFromString(workItem.getEndDateWI());
                        viewHolderCell.wi_time.setText(timeStr);
                        viewHolderCell.iv_type.setImageDrawable(
                                getContext().getDrawable(R.drawable.type_holiday));
                        break;
                }
                viewHolderCell.iv_reminder.setVisibility(
                        (workItem.getReminderWI().equals("") || workItem.isDoneWI()) ? View
                                .INVISIBLE : View.VISIBLE);
                if ((mSelectedItemsIds.get(position))) {
                    viewHolderCell.iv_selected.setBackgroundColor(
                            mContext.getResources().getColor(R.color.selected));
                }
            }
        }
        return view;
    }

    private void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, true);
        } else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }

    @Override
    public void remove(WorkItem object) {
        WorkItemList.remove(object);
        notifyDataSetChanged();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    /* Use view holder for header list view */
    static class ViewHolderHeader {
        @BindView(R.id.label)
        TextView label;

        public ViewHolderHeader(View view) {
            ButterKnife.bind(this, view);
        }
    }

    /* Use view holder for later cache in list view */
    static class ViewHolderCell {
        @BindView(R.id.iv_color)
        ImageView iv_color;
        @BindView(R.id.wi_title)
        TextView wi_title;
        @BindView(R.id.wi_time)
        TextView wi_time;
        @BindView(R.id.iv_type)
        ImageView iv_type;
        @BindView(R.id.iv_reminder)
        ImageView iv_reminder;
        @BindView(R.id.iv_selected)
        LinearLayout iv_selected;

        public ViewHolderCell(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
