package com.dunarctic.timetablemanagement.Database;

/**
 * Created by TrungNhan on 27.12.2017
 */

public class Reminder implements Comparable<Reminder> {
    private String title;
    private String reminderDate;
    private int colorID;
    private int type; //for cancel
    private int nID;

    public Reminder(String title, String reminderDate, int colorID, int nID, int type) {
        this.title = title;
        this.reminderDate = reminderDate;
        this.colorID = colorID;
        this.nID = nID;
        this.type = type;
    }

    @Override
    public int compareTo(Reminder o) {
        return reminderDate.compareTo(o.reminderDate);
    }

    public String getTitle() {
        return title;
    }

    public int getColorID() {
        return colorID;
    }

    public int getType() {
        return type;
    }

    public int getNotificationID() {
        return nID;
    }

    public String getDateTime() {
        return reminderDate;
    }

    public String getTime() {
        return reminderDate.substring(11);
    }

    public String getDate() {
        return reminderDate.substring(0, 10);
    }
}
