package com.dunarctic.timetablemanagement.Database;

import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HEADER_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;

/**
 * Created by dunarctic on 05.12.2017
 */

public class WorkItem implements Comparable<WorkItem> {
    private String title;
    private String startDate;
    private String endDate;
    private boolean isDone;
    private String reminder;
    private int colorId;
    private int type;
    private String description;
    private int notificationID;

    public WorkItem(String title, int type, boolean isDone, String description, String startDate,
                    String endDate, String reminder, int colorId, int nID) {
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reminder = reminder;
        this.isDone = isDone;
        this.colorId = colorId;
        this.type = type;
        this.description = description;
        this.notificationID = nID;
    }

    public WorkItem(String startDate) {
        this.title = "";
        this.startDate = startDate;
        this.endDate = "";
        this.reminder = "";
        this.isDone = false;
        this.colorId = -1;
        this.type = HEADER_TYPE;
        this.description = "";
    }

    @Override
    public int compareTo(WorkItem o) {
        return getStartDateWI().compareTo(o.getStartDateWI());
    }

    public boolean isHeader() {
        return (this.type == HEADER_TYPE);
    }

    public String getTitleWI() {
        return title;
    }

    public String getStartDateWI() {
        return startDate;
    }

    public String getEndDateWI() {
        return endDate;
    }

    public boolean isDoneWI() {
        return isDone;
    }

    public String getReminderWI() {
        return reminder;
    }

    public void setReminderWI(String newReminder) {
        this.reminder = newReminder;
    }

    public int getTypeIdWI() {
        return type;
    }

    public int getColorIdWI() {
        return colorId;
    }

    public void setColorIdWI(int newColor) {
        this.colorId = newColor;
    }

    public int getNotificationID() {
        return notificationID;
    }

    public String getDescriptionWI() {
        return description;
    }

    public void setDescriptionWI(String newDescription) {
        this.description = newDescription;
    }

    public void switchDone() {
        this.isDone = !this.isDone;
    }

    @Override
    public boolean equals(Object obj) {
        WorkItem other = (WorkItem) obj;
        if (other.type != this.type) {
            return false;
        }
        if (other.type == HEADER_TYPE) {
            return (this.startDate.equals(other.startDate));
        }
        if (other.type == DEADLINE_TYPE || other.type == HOLIDAY_TYPE) {
            return (this.title.equals(other.title));
        }
        return (this.title.equals(other.title) &&
                this.startDate.equals(other.startDate) &&
                this.endDate.equals(other.endDate));
    }
}
