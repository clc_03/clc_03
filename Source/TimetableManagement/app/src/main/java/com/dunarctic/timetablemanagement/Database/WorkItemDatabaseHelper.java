package com.dunarctic.timetablemanagement.Database;

import android.app.Activity;
import android.app.Notification;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.provider.BaseColumns;

import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;

import java.util.Calendar;

/**
 * Created by dunarctic on 08.12.2017
 */

public class WorkItemDatabaseHelper extends SQLiteOpenHelper {
    // This table has 8 attributes
    // ID(Integer) | Title(String) | DueDate(Date) | isDone(boolean) | hasReminder(boolean) |
    // Reminder(Date) | ColorID(Integer) | Description(String)
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "WorkItemList.db";
    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + WorkItemTable.TABLE_NAME + " (" +
                    WorkItemTable._ID + " INTEGER PRIMARY KEY," +
                    WorkItemTable.COLUMN_TITLE + " TEXT," +
                    WorkItemTable.COLUMN_START_DATE + " TEXT," +
                    WorkItemTable.COLUMN_END_DATE + " TEXT," +
                    WorkItemTable.COLUMN_IS_DONE + " BOOLEAN," +
                    WorkItemTable.COLUMN_REMINDER + " TEXT," +
                    WorkItemTable.COLUMN_COLOR_ID + " INTEGER," +
                    WorkItemTable.COLUMN_TYPE + " INTEGER," +
                    WorkItemTable.COLUMN_NOTIFICATION + " INTEGER," +
                    WorkItemTable.COLUMN_DESCRIPTION + " TEXT )";

    private static final String SQL_DROP_IF_EXIST =
            "DROP TABLE IF EXISTS " + WorkItemTable.TABLE_NAME;

    public WorkItemDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // add item to the database
    public long insert(String title, String description, String startDate, String endDate,
                       String reminderDate, int colorID, int type, int uNo, Context context) {
        WorkItemDatabaseHelper mdbHelper = new WorkItemDatabaseHelper(context);
        SQLiteDatabase db = mdbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(WorkItemTable.COLUMN_TITLE, title);
        values.put(WorkItemTable.COLUMN_START_DATE, startDate);
        values.put(WorkItemTable.COLUMN_END_DATE, endDate);
        values.put(WorkItemTable.COLUMN_IS_DONE, false);
        values.put(WorkItemTable.COLUMN_REMINDER, reminderDate);
        values.put(WorkItemTable.COLUMN_COLOR_ID, colorID);
        values.put(WorkItemTable.COLUMN_TYPE, type);
        values.put(WorkItemTable.COLUMN_DESCRIPTION, description);
        values.put(WorkItemTable.COLUMN_NOTIFICATION, uNo);
        // insert into a row in database and get new row ID
        return db.insert(WorkItemTable.TABLE_NAME, null, values);
    }

    // add item to the database
    public boolean checkExist(String title, int type, Context mContext) {
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "
                        + WorkItemTable.TABLE_NAME
                        + " WHERE "
                        + WorkItemTable.COLUMN_TITLE
                        + " = '"
                        + title
                        + "' AND "
                        + WorkItemTable.COLUMN_TYPE
                        + " = "
                        + type
                , null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    // remove item in the database: title is unique
    public long removeItem(WorkItem item, Context context) {
        ReminderUtility reminderUti = new ReminderUtility(context);
        DateTimeUtility dtFormat = new DateTimeUtility();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dtFormat.StringToDateTime(item.getReminderWI()));
        Notification set =
                reminderUti.getNotification(item.getTitleWI(), calendar, item.getTypeIdWI(), item.getColorIdWI());
        reminderUti.cancelNotification(set, item.getNotificationID());

        String clause = WorkItemTable.COLUMN_TITLE + " = '" + item.getTitleWI() + "'"
                + " AND " + WorkItemTable.COLUMN_START_DATE + " = '" + item.getStartDateWI() + "'"
                + " AND " + WorkItemTable.COLUMN_END_DATE + " = '" + item.getEndDateWI() + "'"
                + " AND " + WorkItemTable.COLUMN_TYPE + " = " + item.getTypeIdWI();
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        return db.delete(WorkItemTable.TABLE_NAME, clause, null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_IF_EXIST);
        onCreate(db);
    }

    // remove reminder
    public void removeReminder(int mNotificationID, Context mContext) {
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("UPDATE "
                        + WorkItemTable.TABLE_NAME
                        + " SET "
                        + WorkItemTable.COLUMN_REMINDER
                        + " = '' WHERE "
                        + WorkItemTable.COLUMN_NOTIFICATION
                        + " = "
                        + mNotificationID
                , null);
        cursor.moveToFirst();
        cursor.close();
    }

    // update done status all item in the database
    public void updateDone(final Context mContext) {
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Calendar current = Calendar.getInstance();
        DateTimeUtility dtFormat = new DateTimeUtility();
        String currentDateTime = dtFormat.dateTimeToString(current);
        Cursor cursor = db.rawQuery("UPDATE "
                        + WorkItemTable.TABLE_NAME
                        + " SET "
                        + WorkItemTable.COLUMN_IS_DONE
                        + " = "
                        + 1
                        + " WHERE strftime('%s', "
                        + WorkItemTable.COLUMN_END_DATE
                        + ") <= strftime('%s','"
                        + currentDateTime
                        + "')"
                , null);
        cursor.moveToFirst();
        cursor.close();
    }

    // update done reminder after notification
    public void changeReminder(int notificationID, String changedReminder, final Context mContext) {
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("UPDATE "
                        + WorkItemTable.TABLE_NAME
                        + " SET "
                        + WorkItemTable.COLUMN_REMINDER
                        + " = '"
                        + changedReminder
                        + "' WHERE "
                        + WorkItemTable.COLUMN_NOTIFICATION
                        + " = "
                        + notificationID
                , null);
        cursor.moveToFirst();
        cursor.close();
    }

    // update done reminder after notification
    public void updateReminder(final Context mContext) {
        WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        Calendar current = Calendar.getInstance();
        DateTimeUtility dtFormat = new DateTimeUtility();
        String currentDateTime = dtFormat.dateTimeToString(current);
        Cursor cursor = db.rawQuery("UPDATE "
                        + WorkItemTable.TABLE_NAME
                        + " SET "
                        + WorkItemTable.COLUMN_REMINDER
                        + " = '' WHERE strftime('%s', "
                        + WorkItemTable.COLUMN_REMINDER
                        + ") <= strftime('%s','"
                        + currentDateTime
                        + "')"
                , null);
        cursor.moveToFirst();
        cursor.close();
    }

    // remove all done item in the database
    public void removeDone(Context context) {
        new RemoveDoneWorkItem(context).execute();
    }

    public static class WorkItemTable implements BaseColumns {
        public static final String TABLE_NAME = "WorkItemTable";
        // _ID column is already declared in BaseColumns
        public static final String COLUMN_TITLE = "title";
        //empty string for Deadline, Hour = Minute = 00 for Holiday ~ begin of this day
        public static final String COLUMN_START_DATE = "startdate";
        //Hour 23 and Minute = 59 for Holiday ~ end of this day
        public static final String COLUMN_END_DATE = "enddate";
        public static final String COLUMN_IS_DONE = "isdone";
        //empty string for no reminder
        public static final String COLUMN_REMINDER = "reminder";
        // with default is colorPrimaryDark color
        public static final String COLUMN_COLOR_ID = "colorid";
        //0 for deadline; 1 for class; 2 for holiday
        public static final String COLUMN_TYPE = "type";
        //can be empty string
        public static final String COLUMN_DESCRIPTION = "description";
        //unique number for setting notification
        public static final String COLUMN_NOTIFICATION = "notification";
    }

    private class RemoveDoneWorkItem extends AsyncTask<Void, Void, Void> {
        private Context mContext;
        private ProgressDialog progressDialog;

        RemoveDoneWorkItem(Context context) {
            this.mContext = context;
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(context.getString(R.string.in_progress));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            WorkItemDatabaseHelper mDbHelper = new WorkItemDatabaseHelper(mContext);
            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            String clause = WorkItemTable.COLUMN_IS_DONE + " = " + 1;
            db.delete(WorkItemTable.TABLE_NAME, clause, null);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            ((Activity) mContext).recreate();
        }
    }
}
