package com.dunarctic.timetablemanagement.Fragment;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.util.Base64;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Activity.DetailWorkItemActivity;
import com.dunarctic.timetablemanagement.Activity.MainActivity;
import com.dunarctic.timetablemanagement.Adapter.WorkItemListAdapter;
import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Utility.Utility.COLOR_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.DESCRIPTION_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.END_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.HIDE_TABLAYOUT_FAB;
import static com.dunarctic.timetablemanagement.Utility.Utility.IS_DONE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.NID_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.PASSWORD_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REFRESH_CODE;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_CHANGE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_CHANGE_REMIDNER;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_DELETE_ITEM;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_NOT_CHANGE;
import static com.dunarctic.timetablemanagement.Utility.Utility.SHOW_TABLAYOUT_FAB;
import static com.dunarctic.timetablemanagement.Utility.Utility.START_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TITLE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TYPE_KEY;

/**
 * Created by dunarctic on 04.12.2017
 */

public class DayFragment extends Fragment implements com.dunarctic.timetablemanagement.Fragment.FragmentCallBacks {
    public static ArrayList<WorkItem> listWorkItems;
    @BindView(R.id.list_wi)
    ListView mListView; //list of work items
    WorkItemListAdapter WIListAdapter;
    MainActivity activity;

    public static DayFragment newInstance(String StrArg) {
        Bundle args = new Bundle();
        DayFragment fragment = new DayFragment();
        args.putString("TODAY_BUNDLE", StrArg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_today, parent, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // adapter for list view
        WIListAdapter = new WorkItemListAdapter(getContext(), listWorkItems);
        mListView.setAdapter(WIListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // open the detail of each item if clicked
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int curPosition = position;
                //if has password protection
                if (PreferenceManager
                        .getDefaultSharedPreferences(getContext())
                        .getBoolean("security_del", false)) {
                    LayoutInflater li = LayoutInflater.from(getContext());
                    final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    // set prompts.xml to alert dialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.password_et);

                    // set dialog message
                    alertDialogBuilder.setCancelable(false)
                            .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and check it
                                    String curPass = PreferenceManager
                                            .getDefaultSharedPreferences(getContext())
                                            .getString(PASSWORD_KEY, "");
                                    curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                                    if (userInput.getText().toString().equals(curPass)) {
                                        ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                        Intent intent = new Intent(getContext(), DetailWorkItemActivity.class);
                                        WorkItem item = listWorkItems.get(curPosition);
                                        intent.putExtra(TITLE_KEY, item.getTitleWI());
                                        intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                                        intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                                        intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                                        intent.putExtra(REMINDER_KEY, item.getReminderWI());
                                        intent.putExtra(COLOR_KEY, item.getColorIdWI());
                                        intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                                        intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                                        intent.putExtra(NID_KEY, item.getNotificationID());
                                        startActivityForResult(intent, REFRESH_CODE);
                                    } else {
                                        Toast.makeText(getContext(), "Wrong " +
                                                "password", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }).setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alertDialogBuilder.show();
                } else {
                    Intent intent = new Intent(getContext(), DetailWorkItemActivity.class);
                    WorkItem item = listWorkItems.get(curPosition);
                    intent.putExtra(TITLE_KEY, item.getTitleWI());
                    intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                    intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                    intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                    intent.putExtra(REMINDER_KEY, item.getReminderWI());
                    intent.putExtra(COLOR_KEY, item.getColorIdWI());
                    intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                    intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                    intent.putExtra(NID_KEY, item.getNotificationID());
                    startActivityForResult(intent, REFRESH_CODE);
                }
            }
        });
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        // Capture ListView item click
        mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode,
                                                  int position, long id, boolean checked) {
                // Capture total checked items
                final int checkedCount = mListView.getCheckedItemCount();
                // Set the CAB title according to total checked items
                mode.setTitle(checkedCount + " Selected");
                // Calls toggleSelection method from ListViewAdapter Class
                WIListAdapter.toggleSelection(position);
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_delete:
                        final List<WorkItem> deletedList = new ArrayList<>(); //list of work items
                        // Calls getSelectedIds method from ListViewAdapter Class
                        final SparseBooleanArray selected = WIListAdapter.getSelectedIds();
                        if (selected.size() > 0) {
                            AlertDialog.Builder alertDialog_confirm = new AlertDialog.Builder
                                    (getContext());
                            // set information for dialog
                            alertDialog_confirm.setMessage("Are you sure to delete these items?")
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            // it user click cancel
                                            // do nothing, just back the main screen
                                        }
                                    })
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //if has password protection
                                            if (PreferenceManager
                                                    .getDefaultSharedPreferences(getContext())
                                                    .getBoolean("security_del", false)) {
                                                LayoutInflater li = LayoutInflater.from(getContext());
                                                final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                                                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                                                // set prompts.xml to alert dialog builder
                                                alertDialogBuilder.setView(promptsView);

                                                final EditText userInput = (EditText) promptsView
                                                        .findViewById(R.id.password_et);

                                                // set dialog message
                                                alertDialogBuilder.setCancelable(false)
                                                        .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                // get user input and check it
                                                                String curPass = PreferenceManager
                                                                        .getDefaultSharedPreferences(getContext())
                                                                        .getString(PASSWORD_KEY, "");
                                                                curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                                                                if (userInput.getText().toString().equals(curPass)) {
                                                                    ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                                                    for (int i = (selected.size() - 1); i >= 0; i--) {
                                                                        if (selected.valueAt(i)) {
                                                                            WorkItem selecteditem = WIListAdapter
                                                                                    .getItem(selected.keyAt(i));
                                                                            // Remove selected items following the ids
                                                                            WIListAdapter.remove(selecteditem);
                                                                            deletedList.add(selecteditem);
                                                                        }
                                                                    }
                                                                    ((MainActivity) getActivity())
                                                                            .removeListItem(deletedList);
                                                                } else {
                                                                    Toast.makeText(getContext(), "Wrong " +
                                                                            "password", Toast.LENGTH_LONG).show();
                                                                }
                                                            }
                                                        }).setNegativeButton("Cancel",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                                alertDialogBuilder.show();
                                            } else {
                                                for (int i = (selected.size() - 1); i >= 0; i--) {
                                                    if (selected.valueAt(i)) {
                                                        WorkItem selecteditem = WIListAdapter
                                                                .getItem(selected.keyAt(i));
                                                        // Remove selected items following the ids
                                                        WIListAdapter.remove(selecteditem);
                                                        deletedList.add(selecteditem);
                                                    }
                                                }
                                                ((MainActivity) getActivity())
                                                        .removeListItem(deletedList);
                                            }
                                        }
                                    });
                            AlertDialog alert = alertDialog_confirm.create();
                            alert.show();
                        }
                        // Close CAB
                        mode.finish();
                        ((MainActivity) getActivity())
                                .onMsgFromFragmentToMain(SHOW_TABLAYOUT_FAB);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.cab, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                WIListAdapter.removeSelection();
                mode.finish();
                ((MainActivity) getActivity())
                        .onMsgFromFragmentToMain(SHOW_TABLAYOUT_FAB);
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                ((MainActivity) getActivity())
                        .onMsgFromFragmentToMain(HIDE_TABLAYOUT_FAB);
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // when receive change reload data
        WIListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMsgFromMainToFragment(List<WorkItem> list) {

    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_CODE) {
            switch (resultCode) {
                case RESULT_CHANGE_REMIDNER:
                    ReminderUtility reminderUti = new ReminderUtility(getContext());
                    DateTimeUtility dtFormat = new DateTimeUtility();
                    WorkItemDatabaseHelper dbhelper = new WorkItemDatabaseHelper(getContext());

                    String titleStr = data.getStringExtra(TITLE_KEY);
                    String reminderChange = data.getStringExtra(REMINDER_CHANGE_KEY);
                    String reminderStr = data.getStringExtra(REMINDER_KEY);
                    int notificationID = data.getExtras().getInt(NID_KEY);
                    int type = data.getExtras().getInt(TYPE_KEY);
                    int color = data.getExtras().getInt(COLOR_KEY);

                    if (reminderChange.isEmpty()) {
                        //cancel notification
                        Calendar reminderCal = Calendar.getInstance();
                        reminderCal.setTime(dtFormat.StringToDateTime(reminderStr));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderCal, type, color);
                        reminderUti.cancelNotification(set, notificationID);
                        dbhelper.removeReminder(notificationID, getContext());
                    } else {
                        //update notification
                        Calendar reminderChangeCal = Calendar.getInstance();
                        reminderChangeCal.setTime(dtFormat.StringToDateTime(reminderChange));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderChangeCal, type, color);
                        reminderUti.setNotification(set, notificationID, reminderChangeCal);
                        dbhelper.changeReminder(notificationID, reminderChange, getContext());
                    }

                    for (WorkItem item : listWorkItems) {
                        //if there are any checked day
                        if (item.getNotificationID() == notificationID)
                            item.setReminderWI(reminderChange);
                    }

                    WIListAdapter.notifyDataSetChanged();
                    break;
                case RESULT_DELETE_ITEM:
                    String titleStr2 = data.getStringExtra(TITLE_KEY);
                    String startDate2 = data.getStringExtra(START_DATE_KEY);
                    String endDate2 = data.getStringExtra(END_DATE_KEY);
                    String description2 = data.getStringExtra(DESCRIPTION_KEY);
                    String reminderStr2 = data.getStringExtra(REMINDER_KEY);
                    int notificationID2 = data.getExtras().getInt(NID_KEY);
                    int type2 = data.getExtras().getInt(TYPE_KEY);
                    int color2 = data.getExtras().getInt(COLOR_KEY);
                    boolean isDone2 = data.getExtras().getBoolean(IS_DONE_KEY);

                    WorkItem item2 = new WorkItem(titleStr2, type2, isDone2, description2,
                            startDate2, endDate2, reminderStr2, color2, notificationID2);
                    final List<WorkItem> deletedList2 = new ArrayList<>(); //list of work items
                    deletedList2.add(item2);
                    WIListAdapter.remove(item2);
                    ((MainActivity) getActivity())
                            .removeListItem(deletedList2);
                    break;
                case RESULT_NOT_CHANGE:

                    break;
            }
        }
    }
}
