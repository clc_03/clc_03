package com.dunarctic.timetablemanagement.Fragment;

import com.dunarctic.timetablemanagement.Database.WorkItem;

import java.util.List;

/**
 * Created by dunarctic on 27.12.2017
 */

interface FragmentCallBacks {
    void onMsgFromMainToFragment(List<WorkItem> listImage);
}
