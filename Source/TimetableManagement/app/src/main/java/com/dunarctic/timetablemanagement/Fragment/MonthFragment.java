package com.dunarctic.timetablemanagement.Fragment;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dunarctic.timetablemanagement.Activity.DetailWorkItemActivity;
import com.dunarctic.timetablemanagement.Activity.MainActivity;
import com.dunarctic.timetablemanagement.Adapter.WorkItemListAdapter;
import com.dunarctic.timetablemanagement.Database.WorkItem;
import com.dunarctic.timetablemanagement.Database.WorkItemDatabaseHelper;
import com.dunarctic.timetablemanagement.R;
import com.dunarctic.timetablemanagement.Utility.DateTimeUtility;
import com.dunarctic.timetablemanagement.Utility.ReminderUtility;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.COLOR_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.DEADLINE_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.DESCRIPTION_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.END_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.IS_DONE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.NID_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.PASSWORD_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REFRESH_CODE;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_CHANGE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.REMINDER_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_CHANGE_REMIDNER;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_DELETE_ITEM;
import static com.dunarctic.timetablemanagement.Utility.Utility.RESULT_NOT_CHANGE;
import static com.dunarctic.timetablemanagement.Utility.Utility.START_DATE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TITLE_KEY;
import static com.dunarctic.timetablemanagement.Utility.Utility.TYPE_KEY;

/**
 * Created by ASUS on 12/28/2017.
 */

public class MonthFragment extends Fragment implements com.dunarctic.timetablemanagement.Fragment.FragmentCallBacks {

    public static ArrayList<WorkItem> listMonthWorkItems;
    public static ArrayList<WorkItem> listWorkDisplay;
    @BindView(R.id.month_calendar)
    CompactCalendarView calendarView;
    @BindView(R.id.list_wi_monthfrag)
    ListView mListView; //list of work items
    @BindView((R.id.month_text))
    TextView mTextView;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
    private WorkItemListAdapter WIListAdapter;
    private MainActivity activity;
    private DateTimeUtility dateTime;

    public static MonthFragment newInstance(String StrArg) {
        Bundle args = new Bundle();
        MonthFragment fragment = new MonthFragment();
        args.putString("MONTH_BUNDLE", StrArg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        dateTime = new DateTimeUtility();
        listWorkDisplay = new ArrayList<>();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup parent, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.frame_month, parent, false);
        ButterKnife.bind(this, view);
        calendarView.setUseThreeLetterAbbreviation(true);

        mTextView.setText(dateFormat.format(Calendar.getInstance().getTime()));
        if (listMonthWorkItems.isEmpty()) {
            return view;
        } else {
            for (int i = 0; i < listMonthWorkItems.size(); i++) {
                WorkItem currentItem = listMonthWorkItems.get(i);
                int type = currentItem.getTypeIdWI();
                String titleWI = currentItem.getTitleWI();
                String startDateWI = currentItem.getStartDateWI();
                String endDateWI = currentItem.getEndDateWI();
                Date startDate = dateTime.StringToDateTime(startDateWI);
                Date endDate = dateTime.StringToDateTime(endDateWI);
                Event event;
                switch (type) {
                    case DEADLINE_TYPE:
                    case CLASS_TYPE:
                        event = new Event(Color.RED, endDate.getTime(), titleWI);
                        calendarView.addEvent(event);
                        break;
                    case HOLIDAY_TYPE:
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(startDate);
                        String curDate = dateTime.dateTimeToString(startDate);
                        String endDateStr = dateTime.dateTimeToString(endDate);
                        while (!curDate.equals(endDateStr)) {
                            event = new Event(Color.RED, calendar.getTimeInMillis(), titleWI);
                            calendarView.addEvent(event);
                            calendar.add(Calendar.DAY_OF_MONTH, +1);
                            curDate = dateTime.dateTimeToString(calendar.getTime());
                        }
                        //thêm ngày cuối
                        event = new Event(Color.RED, calendar.getTimeInMillis(), titleWI);
                        calendarView.addEvent(event);
                        break;
                }
            }
            calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
                @Override
                public void onDayClick(Date dateClicked) {
                    listWorkDisplay.clear();
                    boolean found = false;
                    for (int i = 0; i < listMonthWorkItems.size(); i++) {
                        WorkItem currentItem = listMonthWorkItems.get(i);
                        int type = currentItem.getTypeIdWI();
                        String startDateWI = currentItem.getStartDateWI();
                        String endDateWI = currentItem.getEndDateWI();
                        Date startDate = dateTime.StringToDateTime(startDateWI);
                        Date endDate = dateTime.StringToDateTime(endDateWI);
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(dateClicked);
                        String date = dateTime.dateTimeToString(dateClicked);
                        boolean ret = false;
                        String start = dateTime.dateTimeToString(startDate);
                        String end = dateTime.dateTimeToString(endDate);
                        switch (type) {
                            case DEADLINE_TYPE:
                            case CLASS_TYPE:
                                ret = (end.compareTo(date) == 0);
                                break;
                            case HOLIDAY_TYPE:
                                ret = (end.compareTo(date) >= 0
                                        && start.compareTo(date) <= 0);
                                break;
                        }
                        if (ret) {
                            listWorkDisplay.add(currentItem);
                            found = true;
                        }
                        WIListAdapter.notifyDataSetChanged();
                    }
                    if (!found) {
                        Toast.makeText(getContext(), "No Events Planned for that day", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onMonthScroll(Date firstDayOfNewMonth) {
                    mTextView.setText(dateFormat.format(firstDayOfNewMonth));
                }
            });
            return view;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // adapter for list view

        WIListAdapter = new WorkItemListAdapter(getContext(), listWorkDisplay);
        mListView.setAdapter(WIListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // open the detail of each item if clicked
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int curPosition = position;
                //if has password protection
                if (PreferenceManager
                        .getDefaultSharedPreferences(getContext())
                        .getBoolean("security_del", false)) {
                    LayoutInflater li = LayoutInflater.from(getContext());
                    final View promptsView = li.inflate(R.layout.dialog_input_password, null);
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    // set prompts.xml to alert dialog builder
                    alertDialogBuilder.setView(promptsView);

                    final EditText userInput = (EditText) promptsView
                            .findViewById(R.id.password_et);

                    // set dialog message
                    alertDialogBuilder.setCancelable(false)
                            .setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // get user input and check it
                                    String curPass = PreferenceManager
                                            .getDefaultSharedPreferences(getContext())
                                            .getString(PASSWORD_KEY, "");
                                    curPass = new String(Base64.decode(curPass, Base64.DEFAULT));
                                    if (userInput.getText().toString().equals(curPass)) {
                                        ((ViewGroup) promptsView.getParent()).removeView(promptsView);
                                        Intent intent = new Intent(getContext(), DetailWorkItemActivity.class);
                                        WorkItem item = listWorkDisplay.get(curPosition);
                                        intent.putExtra(TITLE_KEY, item.getTitleWI());
                                        intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                                        intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                                        intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                                        intent.putExtra(REMINDER_KEY, item.getReminderWI());
                                        intent.putExtra(COLOR_KEY, item.getColorIdWI());
                                        intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                                        intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                                        intent.putExtra(NID_KEY, item.getNotificationID());
                                        startActivityForResult(intent, REFRESH_CODE);
                                    } else {
                                        Toast.makeText(getContext(), "Wrong " +
                                                "password", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }).setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    alertDialogBuilder.show();
                } else {
                    Intent intent = new Intent(getContext(), DetailWorkItemActivity.class);
                    WorkItem item = listWorkDisplay.get(curPosition);
                    intent.putExtra(TITLE_KEY, item.getTitleWI());
                    intent.putExtra(START_DATE_KEY, item.getStartDateWI());
                    intent.putExtra(END_DATE_KEY, item.getEndDateWI());
                    intent.putExtra(IS_DONE_KEY, item.isDoneWI());
                    intent.putExtra(REMINDER_KEY, item.getReminderWI());
                    intent.putExtra(COLOR_KEY, item.getColorIdWI());
                    intent.putExtra(TYPE_KEY, item.getTypeIdWI());
                    intent.putExtra(DESCRIPTION_KEY, item.getDescriptionWI());
                    intent.putExtra(NID_KEY, item.getNotificationID());
                    startActivityForResult(intent, REFRESH_CODE);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // when receive change reload data
        WIListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMsgFromMainToFragment(List<WorkItem> list) {

    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REFRESH_CODE) {
            switch (resultCode) {
                case RESULT_CHANGE_REMIDNER:
                    ReminderUtility reminderUti = new ReminderUtility(getContext());
                    DateTimeUtility dtFormat = new DateTimeUtility();
                    WorkItemDatabaseHelper dbhelper = new WorkItemDatabaseHelper(getContext());

                    String titleStr = data.getStringExtra(TITLE_KEY);
                    String reminderChange = data.getStringExtra(REMINDER_CHANGE_KEY);
                    String reminderStr = data.getStringExtra(REMINDER_KEY);
                    int notificationID = data.getExtras().getInt(NID_KEY);
                    int type = data.getExtras().getInt(TYPE_KEY);
                    int color = data.getExtras().getInt(COLOR_KEY);

                    if (reminderChange.isEmpty()) {
                        //cancel notification
                        Calendar reminderCal = Calendar.getInstance();
                        reminderCal.setTime(dtFormat.StringToDateTime(reminderStr));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderCal, type, color);
                        reminderUti.cancelNotification(set, notificationID);
                        dbhelper.removeReminder(notificationID, getContext());
                    } else {
                        //update notification
                        Calendar reminderChangeCal = Calendar.getInstance();
                        reminderChangeCal.setTime(dtFormat.StringToDateTime(reminderChange));
                        Notification set =
                                reminderUti.getNotification(titleStr, reminderChangeCal, type, color);
                        reminderUti.setNotification(set, notificationID, reminderChangeCal);
                        dbhelper.changeReminder(notificationID, reminderChange, getContext());
                    }

                    for (WorkItem item : listWorkDisplay) {
                        //if there are any checked day
                        if (item.getNotificationID() == notificationID)
                            item.setReminderWI(reminderChange);
                    }

                    WIListAdapter.notifyDataSetChanged();
                    break;
                case RESULT_DELETE_ITEM:
                    String titleStr2 = data.getStringExtra(TITLE_KEY);
                    String startDate2 = data.getStringExtra(START_DATE_KEY);
                    String endDate2 = data.getStringExtra(END_DATE_KEY);
                    String description2 = data.getStringExtra(DESCRIPTION_KEY);
                    String reminderStr2 = data.getStringExtra(REMINDER_KEY);
                    int notificationID2 = data.getExtras().getInt(NID_KEY);
                    int type2 = data.getExtras().getInt(TYPE_KEY);
                    int color2 = data.getExtras().getInt(COLOR_KEY);
                    boolean isDone2 = data.getExtras().getBoolean(IS_DONE_KEY);

                    WorkItem item2 = new WorkItem(titleStr2, type2, isDone2, description2,
                            startDate2, endDate2, reminderStr2, color2, notificationID2);
                    final List<WorkItem> deletedList2 = new ArrayList<>(); //list of work items
                    deletedList2.add(item2);
                    WIListAdapter.remove(item2);
                    ((MainActivity) getActivity())
                            .removeListItem(deletedList2);
                    break;
                case RESULT_NOT_CHANGE:

                    break;
            }
        }
    }
}