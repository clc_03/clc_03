package com.dunarctic.timetablemanagement.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dunarctic on 08.12.2017
 */

public class DateTimeUtility {
    SimpleDateFormat shortDateFormat = new SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault());
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat getWeekFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    private SimpleDateFormat getDateFormat2 = new SimpleDateFormat("yyyy MM dd", Locale
            .getDefault());
    private SimpleDateFormat reminderFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy, HH:mm", Locale.getDefault());
    private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

    public String timeToString(Calendar calendar) {
        return timeFormat.format(calendar.getTime());
    }

    public String dateToString(Calendar calendar) {
        return dateFormat.format(calendar.getTime());
    }

    public String reminderToString(Calendar calendar) {
        return reminderFormat.format(calendar.getTime());
    }

    public String dateTimeToString(Calendar calendar) {
        return dateTimeFormat.format(calendar.getTime());
    }

    public String dateTimeToString(Date date) {
        return getDateFormat2.format(date);
    }

    public Date StringToDateTime(String str) {
        try {
            return dateTimeFormat.parse(str);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public String shortDateFormat(Calendar calendar) {
        return shortDateFormat.format(calendar.getTime());
    }

    public String weekToString(Calendar calendar) {
        String ret = "From week: ";
        Calendar temp = calendar;
        temp.setFirstDayOfWeek(Calendar.MONDAY);

        temp.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        ret += getWeekFormat.format(temp.getTime()) + " - ";

        temp.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        ret += getWeekFormat.format(temp.getTime());
        return ret;
    }

    //set current Calendar's First day of week is Monday
    public void changeWeekdays(Calendar calendar, int idWeekday) { //idWeekday is 0-based
        switch (idWeekday) { //change to selected weekdays
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                calendar.set(Calendar.DAY_OF_WEEK, idWeekday + 2);
                break;
            case 6:
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                break;
        }
    }

    public String formatTime(String str) {
        Calendar cal = Calendar.getInstance();
        Date date = StringToDateTime(str);
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        return dateTimeToString(cal);
    }

    public String getTimeFromString(String str) {
        Calendar cal = Calendar.getInstance();
        Date date = StringToDateTime(str);
        cal.setTime(date);
        return timeToString(cal);
    }

    public String getDateFromString(String str) {
        Calendar cal = Calendar.getInstance();
        Date date = StringToDateTime(str);
        cal.setTime(date);
        return getWeekFormat.format(cal.getTime());
    }
}
