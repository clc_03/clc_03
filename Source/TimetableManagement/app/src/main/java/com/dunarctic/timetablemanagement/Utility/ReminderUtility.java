package com.dunarctic.timetablemanagement.Utility;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.dunarctic.timetablemanagement.R;

import java.util.Calendar;

import static com.dunarctic.timetablemanagement.Utility.ReminderReceiver.NOTIFICATION;
import static com.dunarctic.timetablemanagement.Utility.ReminderReceiver.NOTIFICATION_ID;
import static com.dunarctic.timetablemanagement.Utility.Utility.CLASS_TYPE;
import static com.dunarctic.timetablemanagement.Utility.Utility.HOLIDAY_TYPE;

/**
 * Created by dunarctic on 27.12.2017
 */
//ông mới push nữa hả, nãy t mới pull á
public class ReminderUtility {
    private Context mContext;

    public ReminderUtility(Context context) {
        this.mContext = context;
    }

    public Notification getNotification(String content, Calendar calendar,
                                        int type, int color) {
        DateTimeUtility dateTimeUtility = new DateTimeUtility();
        String showStr = dateTimeUtility.reminderToString(calendar);
        String titleStr = "[" + mContext.getString(R.string.upcoming_event) + "] "
                + content;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        builder.setContentTitle(titleStr)
                .setContentText(showStr)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setColor(color)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(showStr))
                .setShowWhen(false);
        switch (type) {
            case CLASS_TYPE:
                builder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.type_class));
                break;
            case HOLIDAY_TYPE:
                builder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.type_holiday));
                break;
            default:
                builder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.type_deadline));
                break;
        }
        return builder.build();
    }

    // this schedule notification to the time that user set
    public void setNotification(Notification notification, int notificationNumber, Calendar
            dateTime) {
        Intent detailsIntent = new Intent(mContext, ReminderReceiver.class);
        detailsIntent.putExtra(NOTIFICATION_ID, notificationNumber);
        detailsIntent.putExtra(NOTIFICATION, notification);
        PendingIntent detailsPendingIntent = PendingIntent.getBroadcast(mContext, notificationNumber,
                detailsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        dateTime.add(Calendar.SECOND, -30);
        alarmManager.set(AlarmManager.RTC_WAKEUP, dateTime.getTimeInMillis(), detailsPendingIntent);
    }

    // this cancel a future notification if item is deleted or edited
    public void cancelNotification(Notification notification, int notificationNumber) {
        Intent detailsIntent = new Intent(mContext, ReminderReceiver.class);
        detailsIntent.putExtra(NOTIFICATION_ID, notificationNumber);
        detailsIntent.putExtra(NOTIFICATION, notification);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, notificationNumber,
                detailsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}