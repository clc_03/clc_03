package com.dunarctic.timetablemanagement.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import com.dunarctic.timetablemanagement.R;

import java.util.Calendar;

/**
 * Created by dunarctic on 25.12.2017
 */

public class Utility {
    public static final String PASSWORD_KEY = "dunarctic";
    public static final String NOTIFICATION_NUMBER = "notification-number";
    public static final int DEADLINE_TYPE = 0;
    public static final int CLASS_TYPE = 1;
    public static final int HOLIDAY_TYPE = 2;
    public static final int HEADER_TYPE = 11000;
    public static final int REFRESH_CODE = 10001;
    public static final String HIDE_TABLAYOUT_FAB = "hide_tablayoutNfab";
    public static final String SHOW_TABLAYOUT_FAB = "show_tablayoutNfab";

    public static final String TITLE_KEY = "title-key";
    public static final String START_DATE_KEY = "start-date-key";
    public static final String END_DATE_KEY = "end-date-key";
    public static final String IS_DONE_KEY = "is-done-key";
    public static final String REMINDER_KEY = "reminder-key";
    public static final String REMINDER_CHANGE_KEY = "reminder-change-key";
    public static final String COLOR_KEY = "color-key";
    public static final String TYPE_KEY = "type-key";
    public static final String DESCRIPTION_KEY = "description-key";
    public static final String NID_KEY = "nid-key";

    public static final int RESULT_DELETE_ITEM = 11000;
    public static final int RESULT_CHANGE_REMIDNER = 11011;
    public static final int RESULT_NOT_CHANGE = 11111;

    public static Drawable getTodayBackground(Context mContext) {
        Drawable ret;
        Calendar calendar = Calendar.getInstance();
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_monday);
                break;
            case Calendar.TUESDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_tuesday);
                break;
            case Calendar.WEDNESDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_wednesday);
                break;
            case Calendar.THURSDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_thursday);
                break;
            case Calendar.FRIDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_friday);
                break;
            case Calendar.SATURDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_saturday);
                break;
            case Calendar.SUNDAY:
                ret = mContext.getResources().getDrawable(R.drawable.gradient_sunday);
                break;
            default:
                ret = mContext.getResources().getDrawable(R.drawable.default_gradient);
                break;
        }
        return ret;
    }

    public static int getUniqueNumber(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(Activity.class.getSimpleName(),
                Context.MODE_PRIVATE);
        int notificationNumber = prefs.getInt(NOTIFICATION_NUMBER, 0);
        SharedPreferences.Editor editor = prefs.edit();
        notificationNumber++;
        editor.putInt(NOTIFICATION_NUMBER, notificationNumber);
        editor.apply();
        return notificationNumber;
    }
}
